require 'sinatra'
require 'json'
require 'nokogiri'
require 'sinatra/activerecord'
require 'yaml'
require 'faraday'
require 'net/http'
require 'uri'
require 'savon'
require 'digest/md5'
require 'openssl'
require './model'
require './callback'
require './process'
require './api_functions'


STRTRANSFAILURE ="Sorry,your transaction failed"



DEFAULT_PIN ="1234"
API ="API"
MSGSENDERID = "ALEXpay" 
MERCHANT_NUMBER = "261064828"
TRANS_TYPE_DEBIT ="DR"
TRANS_TYPE_CREDIT ="CR"
CALLBACK_URL = "http://67.205.74.208:6088/alex_pay_callback"
VIR_CALLBACK_URL = "http://67.205.74.208:6088/virtual_accounts_callback"
SEND_ALEX_CALLBACK2 = "http://67.205.74.208:6088/send_alex_callback2"
W_VIR_CALLBACK_URL = "http://67.205.74.208:6088/w_virtual_accounts_callback" 
THIRD_PARTY_CALLBACK ="http://67.205.74.208:6088/third_party_callback"
TOP_CALLBACK_URL = "http://67.205.74.208:6088/topUp_callback"
BILL_CALLBACK_URL = "http://67.205.74.208:6088/bill_callback"   
CLIENT_ID = 22
SECRET_KEY ='W0Dz7jXYrkS3QJr5RpnwZGjY8i5PxzuHXhzmiMfLdnM9BePz4RDRPoQibIJ4OLxIOD2UUpRTujcGfTA4dB3Qtw=='
CLIENT_KEY = 'XlnBgzaj/4ZRseV3AratiyjVJeR75Hbhy0wq3y2XIO2bPNZ6jAct5XIC70oovj3kJf4oygF6kcgIzAbbwc7kvw=='
API_KEY = 'ssbeIdOOtChW/BRGiazOZ1d7/NTUnyXW2aTl7YmLrHthUlgZfZhxuM5o/T8o02zcpdXKDWVi6y5J7qG5pUnyfw=='
MERCHANT_NO = "261064828"
TIG_MERCHANT = "0271300376"
MTN_MERCHANT = ""
VOD_MERCHANT = ""


set :bind, '67.205.74.208'
set :port, '6088'  



LOGIN_SUC ={ resp_code: '200', resp_desc: "Login successful" }
PIN_CHANGE_SUC ={ resp_code: '200', resp_desc: "PIN changed successful. Please login again to continue." }
LOGIN_FAIL ={ resp_code: '201', resp_desc: "Login failed. Wrong mobile number or PIN. Please try again." }
LOGIN_FAILL ={ resp_code: '201', resp_desc: "Old PIN incorrect. Please try again." }
SUC_REGIS ={ resp_code: '202', resp_desc: "You have already subscribed to ALEXpay!" }
ERR_FIRST_NAME = { resp_code: '101', resp_desc: 'Please enter first name' }
ERR_LAST_NAME = { resp_code: '102', resp_desc: 'Please enter last name' }
ERR_MOBILE = { resp_code: '103', resp_desc: 'Please enter mobile number' }
ERR_ID_TYPE = { resp_code: '104', resp_desc: 'Please select an id type' }
ERR_ID_NUM= { resp_code: '105', resp_desc: 'Please enter an id number' }
ERR_RECIPIENT_NUM = { resp_code: '106', resp_desc: 'Please enter a recipient number' }
ERR_TOPUP_OPTION = { resp_code: '107', resp_desc: 'Please select top up option' }
ERR_SENDER = { resp_code: '108', resp_desc: 'Please enter sender name' }
ERR_BILL_TYPE = { resp_code: '109', resp_desc: 'Please select a bill type' }
ERR_SMART_CARD = { resp_code: '110', resp_desc: 'Please enter a smart card number' }
ERR_RECIPIENT_NET ={ resp_code: '111', resp_desc: 'Please select a recipient network' }
ERR_NETWORK = { resp_code: '112', resp_desc: 'Please select a network' }
ERR_AMOUNT = { resp_code: '113', resp_desc: 'Please enter an amount' }
ERR_AMOUNTT = { resp_code: '113', resp_desc: 'Top up amount should not be less than GHc 0.2' }
ERR_AMOUNTTT = { resp_code: '113', resp_desc: 'Top up amount should not be less than GHc 1' }
NO_DATA = {resp_code: '117', resp_desc: "Transfer history not available"}
ERR_PIN = {resp_code: '118', resp_desc: "PIN should be 4-digits"}
ALREADY_REGIS = {resp_code: '118', resp_desc: "You have already subscribed to ALEXpay"} 

VER_CODE = {resp_code: "801", resp_desc: "Verification Code Sent Successfully"}
ERR_FAKE = {resp_code: "809", resp_desc: "Verification Code provided is invalid!"}
ERR_VER_FAILED = {resp_code: "810", resp_desc: "Verification Failed!"}
VER_SUCCESS = {resp_code: "808", resp_desc: "User verified successfully"}
ERR_VERIFY = {resp_code: "804", resp_desc: "There was a problem verifying user"} 


#login
post '/ap_register' do
  
  puts"RUNNING SUBSCRIPTION"
  
  request.body.rewind
  req = JSON.parse request.body.read
   response['Access-Control-Allow-Origin'] = '*'
  
  first_name = req['first_name']
   last_name = req['last_name']
   mobile_number = req['msisdn']
   telco = req['telco']
   alt_mobile_number = req['alt_mobile_number']
  
  
 #########VALIDATING THE PARAMS 
  
  halt ERR_FIRST_NAME.to_json unless req['first_name']
  halt ERR_LAST_NAME.to_json unless req['last_name']
  halt ERR_MOBILE.to_json unless req.key?('msisdn')
  halt ERR_NETWORK.to_json unless req['telco']
  halt ERR_MOBILE.to_json unless req.key?('telco')
 
  

   if first_name.length < 2 
     halt ERR_FIRST_NAME.to_json
   elsif last_name.length< 2
      halt ERR_LAST_NAME.to_json
   elsif mobile_number.length<10
     halt ERR_MOBILE.to_json
   
   end

  
    if userExists(mobile_number)
      #Subscription.exists?(:msisdn => req['msisdn'])
      halt ALREADY_REGIS.to_json
   
    else  
      
       _authCode = generateAuthCode
       _sub= subscriber_id       
     puts insert_subscribe(mobile_number, first_name,last_name,alt_mobile_number,_sub,pin_hash(DEFAULT_PIN),telco,_authCode)
     
      # pin_save(mobile_number,DEFAULT_PIN,_sub)    
      amount = 0.00 
      virtual_wallet(first_name,last_name,mobile_number,amount)   
       _strMessage = "Your ALEXpay verification code is #{_authCode}."
       
          Thread.new{
            send_sms("ALEXpay", mobile_number, _strMessage)
          }
          VER_CODE.to_json
      
       # strText2 ="You have successfully registered on ALEXpay! Your default ALEXpay PIN is 1234. Please go to 'Profile' and change your PIN"
       # puts send_sms(MSGSENDERID, mobile_number, strText2)
          
   
    end
    VER_CODE.to_json     
end


post '/user_verify' do 
  
  puts"RUNNING SUBSCRIPTION"
  
  request.body.rewind
  req = JSON.parse request.body.read
   response['Access-Control-Allow-Origin'] = '*'
   
  mobile_number = req['mobile_number']
  _authCode = req['auth_code']
  updateStatus = updateUserStatus(mobile_number,_authCode)
  if updateStatus.to_i > 0 
    _sub= subscriber_id  
    pin_save(mobile_number,DEFAULT_PIN,_sub) 
      strText2 ="You have successfully registered on ALEXpay! Your default ALEXpay PIN is 1234. Please go to 'Profile' and change your PIN"
       puts send_sms(MSGSENDERID, mobile_number, strText2)
   halt VER_SUCCESS.to_json
    else
   halt ERR_VERIFY.to_json
  end
end



post '/login' do
  
  request.body.rewind
  req = JSON.parse request.body.read
   response['Access-Control-Allow-Origin'] = '*'
  
  
   mobile_number = req['msisdn']
   pin = req['pin']
  
  
 #########VALIDATING THE PARAMS 
  
 
  halt ERR_MOBILE.to_json unless req.key?('msisdn')
  halt ERR_PIN.to_json unless req.key?('pin')
 
  

  
   if mobile_number.length<10
     halt ERR_MOBILE.to_json
   elsif pin.length <4
     halt ERR_PIN.to_json
   
   end
 
    if userExists(mobile_number)
      #  Subscription.exists?(:msisdn => req['msisdn'])
       old_pin = PinTable.where(' msisdn=? and alt_status=?',mobile_number,0).order('id desc')[0].pin

      default_pin = Subscription.where(' msisdn=? and status=?',mobile_number,0).order('id desc')[0].pin
      
      puts "--------"
      puts "LETS SEE PIN #{pin}"
      puts "LETS SEE PIN HASH #{pin_hash(pin)}"
      puts "LETS SEE OLD PIN #{old_pin}"
      puts "LETS SEE DEFAULT PIN #{default_pin}"
      
    if  pin_hash(pin) == old_pin
      halt LOGIN_SUC.to_json
       puts "PIN CORRECT"
     
     else 
       halt LOGIN_FAIL.to_json
       puts  puts "PIN INCORRECT"
       
     end
      
      halt SUC_REGIS.to_json
   
    else  
      regis_success  =  { resp_code: '222',resp_desc: 'Please subscribe to ALEXpay to continue'}  
    end
    regis_success.to_json       
end


post '/change_pin' do
  
  request.body.rewind
  req = JSON.parse request.body.read
   response['Access-Control-Allow-Origin'] = '*'
  
  
   mobile_number = req['msisdn']
   pin = req['pin']
   new_pin = req['new_pin']
  
  
 #########VALIDATING THE PARAMS 
  
 
  halt ERR_MOBILE.to_json unless req.key?('msisdn')
  halt ERR_PIN.to_json unless req.key?('pin')
  halt ERR_PIN.to_json unless req.key?('new_pin')
 
  

  
   if mobile_number.length<10
     halt ERR_MOBILE.to_json
   elsif pin.length <4
     halt ERR_PIN.to_json
   
   end
 
    if userExists(mobile_number)
      #Subscription.exists?(:msisdn => req['msisdn'])
       old_pin = PinTable.where(' msisdn=? and alt_status=?',mobile_number,0).order('id desc')[0].pin

      default_pin = Subscription.where(' msisdn=? and status=?',mobile_number,0).order('id desc')[0].pin
      
      puts "--------"
      puts "LETS SEE PIN #{pin}"
      puts "LETS SEE NEW PIN #{new_pin}"
      puts "LETS SEE PIN HASH #{pin_hash(pin)}"
      puts "LETS SEE OLD PIN #{old_pin}"
      puts "LETS SEE DEFAULT PIN #{default_pin}"
      
    if  pin_hash(pin) == old_pin
      
      save_pin(mobile_number,new_pin)

          strText2 ="Alert! Your ALEXpay PIN has been changed. Your new pin is #{new_pin} "
          puts send_sms(MSGSENDERID, mobile_number, strText2)
       puts "PIN CORRECT"
     halt PIN_CHANGE_SUC.to_json
     else 
       puts  puts "PIN INCORRECT"
       halt LOGIN_FAILL.to_json
       
       
     end
      
      halt SUC_REGIS.to_json
   
    else  
      regis_success  =  { resp_code: '222',resp_desc: 'Please subscribe to ALEXpay to continue'}  
    end
    regis_success.to_json       
end



post '/ap_transfer_networks' do 
  request.body.rewind
  req = JSON.parse request.body.read
  
  response['Access-Control-Allow-Origin'] = '*'
  
  network = req['telco']
  mobile_number = req['mobile_number']
 amount = req['amount']
  recipient_number = req['recipient_number']
  recipient_network = req['recipient_telco']
  sender_name = req['sender_name']
  voucher_code = req['voucher_code']
  
  
  #########VALIDATING THE PARAMS 
  
  halt ERR_NETWORK.to_json unless req['telco']
 halt ERR_AMOUNT.to_json unless req['amount']
  halt ERR_MOBILE.to_json unless req.key?('mobile_number')
  halt ERR_RECIPIENT_NUM.to_json unless req.key?('recipient_number')
  halt ERR_RECIPIENT_NET.to_json unless req.key?('recipient_telco')
  halt ERR_SENDER.to_json unless req.key?('sender_name')
  

   if network.length < 2 
     halt ERR_NETWORK.to_json
  elsif amount.to_f < 0.1
     halt ERR_AMOUNT.to_json
   elsif mobile_number.length<10
     halt ERR_MOBILE.to_json
    elsif recipient_number.length<2
      halt ERR_RECIPIENT_NUM.to_json
    elsif recipient_network.length<2 
     halt ERR_RECIPIENT_NET.to_json
    elsif sender_name.length<2
    halt ERR_SENDER.to_json 
   end
   
  
  if  userExists(mobile_number)
    # Subscription.exists?(:msisdn => req['mobile_number'])
    puts ""
    puts "RUnning process payments"
    _return_value = processPayment(mobile_number,network,amount,recipient_number,recipient_network,sender_name ,voucher_code)
    puts "Value: #{_return_value}"
    puts "RUnning after"
   regis_success  =  { resp_code: '000',resp_desc: 'Transfer successful'}
  else
     regis_success  =  { resp_code: '222',resp_desc: 'You havent subscribed to ALEXpay'}
  end
  
  regis_success.to_json  
end

post '/send_others_alexpay' do 
  request.body.rewind
  req = JSON.parse request.body.read
   response['Access-Control-Allow-Origin'] = '*'
  
 
 
  network = req['telco']
  mobile_number = req['mobile_number']
 amount = req['amount']
  recipient_number = req['recipient_number']
  recipient_network = req['recipient_telco']
  sender_name = req['sender_name']
  voucher_code = req['voucher_code'] 
  
  
  #########VALIDATING THE PARAMS 
  
  halt ERR_NETWORK.to_json unless req['telco']
 halt ERR_AMOUNT.to_json unless req['amount']
  halt ERR_MOBILE.to_json unless req.key?('mobile_number')
  halt ERR_RECIPIENT_NUM.to_json unless req.key?('recipient_number')
  halt ERR_RECIPIENT_NET.to_json unless req.key?('recipient_telco')
  halt ERR_SENDER.to_json unless req.key?('sender_name')
  

   if network.length < 2 
     halt ERR_NETWORK.to_json
  elsif amount.to_f < 0.1
     halt ERR_AMOUNT.to_json
   elsif mobile_number.length<10
     halt ERR_MOBILE.to_json
    elsif recipient_number.length<2
      halt ERR_RECIPIENT_NUM.to_json
    elsif recipient_network.length<2 
     halt ERR_RECIPIENT_NET.to_json
    elsif sender_name.length<2
    halt ERR_SENDER.to_json 
   end
  
   if Subscription.exists?(:msisdn => req['mobile_number'])
   puts ""
    puts "RUnning sendOtherALex"
    sendOtherALex(mobile_number,network,amount,recipient_number,recipient_network,sender_name,voucher_code)
   puts "RUnning after"
   
    self_virtual_success  =  { resp_code: '000',resp_desc: 'Other deposit (Virtual Wallet) successful'}
  else
     self_virtual_success  =  { resp_code: '222',resp_desc: 'You havent subscrbed to ALEXpay'}
  end
  
  self_virtual_success.to_json  
  
end




post '/send_others_alexpay2' do 
  request.body.rewind
  req = JSON.parse request.body.read
   response['Access-Control-Allow-Origin'] = '*'
  
 
 
  network = req['telco']
  mobile_number = req['mobile_number']
 amount = req['amount']
  recipient_number = req['recipient_number']
  recipient_network = req['recipient_telco']
  sender_name = req['sender_name']
  voucher_code = req['voucher_code'] 
  
  
  #########VALIDATING THE PARAMS 
  
  halt ERR_NETWORK.to_json unless req['telco']
 halt ERR_AMOUNT.to_json unless req['amount']
  halt ERR_MOBILE.to_json unless req.key?('mobile_number')
  halt ERR_RECIPIENT_NUM.to_json unless req.key?('recipient_number')
  halt ERR_RECIPIENT_NET.to_json unless req.key?('recipient_telco')
  halt ERR_SENDER.to_json unless req.key?('sender_name')
  

   if network.length < 2 
     halt ERR_NETWORK.to_json
  elsif amount.to_f < 0.1
     halt ERR_AMOUNT.to_json
   elsif mobile_number.length<10
     halt ERR_MOBILE.to_json
    elsif recipient_number.length<2
      halt ERR_RECIPIENT_NUM.to_json
    elsif recipient_network.length<2 
     halt ERR_RECIPIENT_NET.to_json
    elsif sender_name.length<2
    halt ERR_SENDER.to_json 
   end
  
   if Subscription.exists?(:msisdn => req['mobile_number'])
   puts ""
    puts "RUnning sendOtherALex"
    sendOtherALex2(mobile_number,network,amount,recipient_number,recipient_network,sender_name,voucher_code)
   puts "RUnning after"
   
    self_virtual_success  =  { resp_code: '000',resp_desc: 'Other deposit (Virtual Wallet) successful'}
  else
     self_virtual_success  =  { resp_code: '222',resp_desc: 'You havent subscrbed to ALEXpay'}
  end
  
  self_virtual_success.to_json  
  
end


post '/self_virtual_deposits' do 
  request.body.rewind
  req = JSON.parse request.body.read
   response['Access-Control-Allow-Origin'] = '*'
  
  network = req['telco']
  mobile_number = req['mobile_number']
  amount = req['amount']
  sender_name = req['sender_name']
  voucher_code = req['voucher_code']
  
  
   #########VALIDATING THE PARAMS 
  
  halt ERR_NETWORK.to_json unless req['telco']
  halt ERR_AMOUNT.to_json unless req['amount']
  halt ERR_MOBILE.to_json unless req.key?('mobile_number')
  halt ERR_SENDER.to_json unless req.key?('sender_name')
  

   if network.length < 2 
     halt ERR_NETWORK.to_json
   elsif amount.to_f < 0.1
      halt ERR_AMOUNT.to_json
   elsif mobile_number.length<10
     halt ERR_MOBILE.to_json
    elsif sender_name.length<2
     halt ERR_SENDER.to_json
   end
  
   if Subscription.exists?(:msisdn => req['mobile_number'])
   puts ""
    puts "RUnning virtualprocessPayment"
  virtualprocessPayment(mobile_number,network,amount,sender_name,voucher_code)
   puts "RUnning after"
   
    self_virtual_success  =  { resp_code: '000',resp_desc: 'Self deposit (Virtual Wallet) successful'}
  else
     self_virtual_success  =  { resp_code: '222',resp_desc: 'You havent subscrbed to ALEXpay'}
  end
  
  self_virtual_success.to_json  
  
end

post '/thirdparty_virtual_deposits' do 
  request.body.rewind
  req = JSON.parse request.body.read
  
   response['Access-Control-Allow-Origin'] = '*'
  
 network = req['telco']
  mobile_number = req['mobile_number']
  amount = req['amount']
  recipient_number = req['recipient_number']
  recipient_network = req['recipient_telco']
  sender_name = req['sender_name']
  voucher_code = req['voucher_code']
  
  
   #########VALIDATING THE PARAMS 
  
  halt ERR_NETWORK.to_json unless req['telco']
  halt ERR_AMOUNT.to_json unless req['amount']
  halt ERR_MOBILE.to_json unless req.key?('mobile_number')
  halt ERR_RECIPIENT_NUM.to_json unless req.key?('recipient_number')
  halt ERR_RECIPIENT_NET.to_json unless req.key?('recipient_telco')
  halt ERR_SENDER.to_json unless req.key?('sender_name')
  

   if network.length < 2 
     halt ERR_NETWORK.to_json
   elsif amount.to_f < 0.1
      halt ERR_AMOUNT.to_json
   elsif mobile_number.length<10
     halt ERR_MOBILE.to_json
    elsif recipient_number.length<2
      halt ERR_RECIPIENT_NUM.to_json
    elsif recipient_network.length<2 
     halt ERR_RECIPIENT_NET.to_json
    elsif sender_name.length<2
     halt ERR_SENDER.to_json
   end
  
   if Subscription.exists?(:msisdn => req['mobile_number'])
   puts ""
    puts "RUnning third_partyPayment"
  third_party_processPayment(mobile_number,network,amount,recipient_number,recipient_network,sender_name,voucher_code) 
   puts "RUnning after"
   
    self_virtual_success  =  { resp_code: '000',resp_desc: 'Third-party deposit (Virtual Wallet) successful'}
  else
     self_virtual_success  =  { resp_code: '222',resp_desc: 'You havent subscrbed to ALEXpay'}
  end
  
  self_virtual_success.to_json  
  
end

post '/self_virtual_withdrawal' do 
  request.body.rewind
  req = JSON.parse request.body.read
  
   response['Access-Control-Allow-Origin'] = '*'
  
  network = req['telco']
  mobile_number = req['mobile_number']
  amount = req['amount']
  sender_name = req['sender_name']
  voucher_code = " "
  
   #########VALIDATING THE PARAMS 
  
  halt ERR_NETWORK.to_json unless req['telco']
  halt ERR_AMOUNT.to_json unless req['amount']
  halt ERR_MOBILE.to_json unless req.key?('mobile_number')
 
  halt ERR_SENDER.to_json unless req.key?('sender_name')
  

   if network.length < 2 
     halt ERR_NETWORK.to_json
   elsif amount.to_f < 0.1
      halt ERR_AMOUNT.to_json
   elsif mobile_number.length<10
     halt ERR_MOBILE.to_json
    elsif sender_name.length<2
     halt ERR_SENDER.to_json
   end

   
   if Subscription.exists?(:msisdn => req['mobile_number'])
   puts ""
    puts "self_virtual_withdrawal"
   self_withdrawal_virtual(mobile_number,network,amount,sender_name,voucher_code)
   puts "RUnning after"
   
    self_virtual_withdrawal_success  =  { resp_code: '000',resp_desc: 'Self withdrawal deposit (Virtual Wallet) successful'}
  else
     self_virtual_withdrawal_success  =  { resp_code: '222',resp_desc: 'You havent subscrbed to ALEXpay'}
  end
  
  self_virtual_withdrawal_success.to_json  
  
end

post '/self_top_up' do 
  request.body.rewind
  req = JSON.parse request.body.read
  
   response['Access-Control-Allow-Origin'] = '*'
  
  network = req['telco']
  mobile_number = req['mobile_number']
  amount = req['amount']
  sender_name = req['sender_name']
  top_up_option = req['top_up_option']
  voucher_code = req['voucher_code']
  
   #########VALIDATING THE PARAMS 
  
  halt ERR_NETWORK.to_json unless req['telco']
  halt ERR_AMOUNT.to_json unless req['amount']
  halt ERR_MOBILE.to_json unless req.key?('mobile_number')
  halt ERR_TOPUP_OPTION.to_json unless req.key?('top_up_option')
  halt ERR_SENDER.to_json unless req.key?('sender_name')
  

   if network.length < 2 
     halt ERR_NETWORK.to_json
   elsif amount.to_f < 0.2
      halt ERR_AMOUNTT.to_json
   elsif mobile_number.length<10
     halt ERR_MOBILE.to_json
    elsif top_up_option != "Self"  
     halt ERR_TOPUP_OPTION.to_json
    elsif sender_name.length<2
     halt ERR_SENDER.to_json
   end

   
   if Subscription.exists?(:msisdn => req['mobile_number'])
   puts ""
    puts "self_top_up"
   self_topUpPayment(mobile_number,network,amount,sender_name,voucher_code,top_up_option)
   puts "RUnning after"
   
    self_topup_success  =  { resp_code: '000',resp_desc: 'Self airtime topup successful'}
  else
     self_topup_success  =  { resp_code: '222',resp_desc: 'You havent subscrbed to ALEXpay'}
  end
  
  self_topup_success.to_json  
  
end

post '/other_top_up' do  
  request.body.rewind
  req = JSON.parse request.body.read
  
   response['Access-Control-Allow-Origin'] = '*'
  
  network = req['telco']
  mobile_number = req['mobile_number']
  amount = req['amount']
  sender_name = req['sender_name']
  recipient_number = req['recipient_number']
  top_up_option = req['top_up_option']
  voucher_code = req['voucher_code']
  
  
    #########VALIDATING THE PARAMS 
  
  halt ERR_NETWORK.to_json unless req['telco']
  halt ERR_AMOUNT.to_json unless req['amount']
  halt ERR_MOBILE.to_json unless req.key?('mobile_number')
  halt ERR_RECIPIENT_NUM.to_json unless req.key?('recipient_number')
  halt ERR_TOPUP_OPTION.to_json unless req.key?('top_up_option')
  halt ERR_SENDER.to_json unless req.key?('sender_name')
  

   if network.length < 2 
     halt ERR_NETWORK.to_json
   elsif amount.to_f < 0.2
      halt ERR_AMOUNTT.to_json
   elsif mobile_number.length<10
     halt ERR_MOBILE.to_json
    elsif recipient_number.length<2
      halt ERR_RECIPIENT_NUM.to_json
    elsif top_up_option != "Others" 
     halt ERR_TOPUP_OPTION.to_json
    elsif sender_name.length<2
     halt ERR_SENDER.to_json
   end
  

   
   if Subscription.exists?(:msisdn => req['mobile_number'])
   puts ""
    puts "others topup"
   other_topUpPayment(mobile_number,network,amount,recipient_number,sender_name,voucher_code,top_up_option)
   puts "RUnning after"
   
    others_success  =  { resp_code: '000',resp_desc: 'Others topup successful'}
  else
     others_success  =  { resp_code: '222',resp_desc: 'You havent subscrbed to ALEXpay'}
  end
  
  others_success.to_json  
  
end

post '/bill_payment' do 
  request.body.rewind
  req = JSON.parse request.body.read
   response['Access-Control-Allow-Origin'] = '*'
  
  network = req['telco']
  mobile_number = req['mobile_number']
  smart_card_number = req['smart_card_num']
  amount = req['amount']  
  bill_type = req['bill_type']
  sender_name = req['sender_name']
  voucher_code = req['voucher_code']
  
  
  #########VALIDATING THE PARAMS 
  
  halt ERR_NETWORK.to_json unless req['telco']
  halt ERR_AMOUNT.to_json unless req['amount']
  halt ERR_MOBILE.to_json unless req.key?('mobile_number')
  halt ERR_BILL_TYPE.to_json unless req.key?('bill_type')
  halt ERR_SMART_CARD.to_json unless req.key?('smart_card_num')
  halt ERR_SENDER.to_json unless req.key?('sender_name')
  

   if network.length < 2 
     halt ERR_NETWORK.to_json
   elsif amount.to_f < 1.0
      halt ERR_AMOUNTTT.to_json
   elsif mobile_number.length<10
     halt ERR_MOBILE.to_json
    elsif bill_type.length<3
     halt ERR_BILL_TYPE.to_json
    elsif smart_card_number.length<10
     halt ERR_SMART_CARD.to_json
    elsif sender_name.length<2
     halt ERR_SENDER.to_json
   end

   
   if Subscription.exists?(:msisdn => req['mobile_number'])
   puts ""
    puts "Bill Payment running"
   billPayment(mobile_number,network,amount,sender_name,voucher_code,bill_type,smart_card_num)
   puts "RUnning after"
   
    bill_payment_success =  { resp_code: '000',resp_desc: 'Bill payment successful'}
  else
     bill_payment_success  =  { resp_code: '222',resp_desc: 'You havent subscrbed to ALEXpay'}
  end
  
  bill_payment_success.to_json  
  
end

post '/check_balance' do
  
  
      request.body.rewind
  req = JSON.parse request.body.read
  
  response['Access-Control-Allow-Origin'] = '*'

  mobile_number = req['mobile_number']
  pin = req['pin']
 
  #########VALIDATING THE PARAMS 

  halt ERR_MOBILE.to_json unless req.key?('mobile_number')
  
   if mobile_number.length<10
     halt ERR_MOBILE.to_json
    
    elsif pin.length <4
     halt ERR_PIN.to_json
   
   end
 
  if Subscription.exists?(:msisdn => req['mobile_number'])
    
       old_pin = PinTable.where(' msisdn=? and alt_status=?',mobile_number,0).order('id desc')[0].pin

      default_pin = Subscription.where(' msisdn=? and status=?',mobile_number,0).order('id desc')[0].pin
      
      puts "--------"
      puts "LETS SEE PIN #{pin}"
      puts "LETS SEE PIN HASH #{pin_hash(pin)}"
      puts "LETS SEE OLD PIN #{old_pin}"
      puts "LETS SEE DEFAULT PIN #{default_pin}"
      
    if  pin_hash(pin) == old_pin
       puts ""
    puts "RUnning check balance"
    
    old_amount = CustomerVirtualWallet.where(mobile_number: mobile_number)[0]
    
    puts "RUnning after"
   regis_success  =  { resp_code: '000',resp_desc: "Your ALEXpay Account balance is GH#{old_amount.amount}\n"}
     
     else 
       regis_success  =  { resp_code: '001',resp_desc: "Your ALEXpay PIN is incorrect.\n"}
       # halt LOGIN_FAIL.to_json
       puts  puts "PIN INCORRECT"
       
     end

   
  else
     regis_success  =  { resp_code: '222',resp_desc: 'This mobile number is not subscribed to ALEXpay'}
  end
  
  regis_success.to_json  
  
end

post '/transfer_history1' do
  
   request.body.rewind
 req= JSON.parse request.body.read
   
  response['Access-Control-Allow-Origin'] = '*'

  mobile_number = req['mobile_number']
  
  puts "THE MOBILE NUMBER IS #{mobile_number}"
 
 
  if Subscription.exists?(:msisdn => req['mobile_number'])
   
    
  
  history = TransactionSummary.transfer_history1(mobile_number) 
   p history.to_json
  p "---------------------++++++==============================================================_______________"
  # p JSON.parse(history)

  if history
    format_response(history, request.accept)
    history.to_json  
  else
    halt NO_DATA.to_json
  end

    regis_success  =  { resp_code: '000',resp_desc: "History Successfull\n"}
  else
     regis_success  =  { resp_code: '222',resp_desc: 'This mobile number is not subscribed to ALEXpay'}
   end
#   
   regis_success.to_json  
  
end


post '/retrieve_sub' do
  
    request.body.rewind
  req = JSON.parse request.body.read
   
  response['Access-Control-Allow-Origin'] = '*' 
  
  mobile_number = req['msisdn']
  
  puts "THE MOBILE NUMBER IS #{mobile_number}"
  
  if Subscription.exists?(:msisdn => req['msisdn'])


    if Subscription.retrieve_sub(mobile_number).exists?
        retrieve = Subscription.retrieve_sub(mobile_number) 
    p "-----------------------------"
    p retrieve 
    p retrieve.to_json
    
     return retrieve.to_json
      
    
      else
        halt NO_DATA.to_json
    end
    
 else
   check  =  { resp_code: '222',resp_desc: 'This mobile number is not subscribed to ALEXpay'}
 end
  
return check.to_json 
  
end



post '/transfer_history' do
  
   request.body.rewind
  req = JSON.parse request.body.read
   
  response['Access-Control-Allow-Origin'] = '*' 
  
  mobile_number = req['mobile_number']
  
  puts "THE MOBILE NUMBER IS #{mobile_number}"
  
  if Subscription.exists?(:msisdn => req['mobile_number'])


    if TransactionSummary.transfer_history1(mobile_number).exists?
        history = TransactionSummary.transfer_history1(mobile_number) 
    p "-----------------------------"
    p history 
    p history.to_json
    
     return history.to_json
      
    
      else
        halt NO_DATA.to_json
    end
    
 else
   check  =  { resp_code: '222',resp_desc: 'This mobile number is not subscribed to ALEXpay'}
 end
  
return check.to_json 

 
end


