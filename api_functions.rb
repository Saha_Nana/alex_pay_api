STROPENTIMEOUT="180"

URL='http://184.173.139.74:8198'
HEADER={'Content-Type'=>'Application/json','timeout'=>'180'}

STRREF = "ALEXpay"
new_HEADERS = {'Content-Type'=> 'application/json','timeout'=>STROPENTIMEOUT, 'open_timeout'=>STROPENTIMEOUT}
REQHDR = {'Content-Type'=>'Application/json','timeout'=>'180'}


def userExists(mobile_number)
  return Subscription.where(msisdn: mobile_number, verified: 1, status: 1).exists?
end

def generateAuthCode
    authCode = loop do
      token = rand(999999).to_s.center(6, rand(9).to_s)
      break token unless AuthCode.exists?(auth_code: token)
    end
  insertAuthCode(authCode)
  puts "Verification Code: #{authCode}"
  authCode
end

def insertAuthCode(authCode)
 puts AuthCode.create!(
    auth_code: authCode,
    status: true
  )
end


def updateUserStatus(mobile_number,auth_code)
   # mobile_number = validatePhoneNumber(mobile_number)
   puts "MObile number is #{mobile_number} and auth code is #{auth_code}"
  puts _boolean = AuthCode.where(auth_code: auth_code).exists?
  puts "LETS SEE BOOLEAN #{_boolean}"
  if _boolean
    if Subscription.where(auth_code: auth_code, msisdn: mobile_number).exists?
      _obj =  Subscription.where(msisdn: mobile_number,verified: 0,auth_code: auth_code).order('id desc').update_all(verified: 1, status: 1)
     
      puts "---Update Status---"
      puts _obj
      _obj
     else
       halt ERR_VER_FAILED.to_json
    end 
    else
     halt ERR_FAKE.to_json 
  end
 
end




def update_trans_status(trans_ref,field)
  res = TransactionSummary.where(ack_code: trans_ref).last
  puts
  puts "LETS SEE RESPONSE IN UPDATE"
  if res
    if field== "success"
    res.status = 1
    res.save
   
    elsif field == "fail"
      res.status = 0
    res.save
    end
  else
puts "Record not found"
  end
  
end



def update_trans_status_cr(trans_ref,field)
  rev_res = TransactionSummary.where(ack_code: trans_ref).first
  if rev_res
    if field== "success"
    rev_res.status = 1
    rev_res.save
   
    elsif field == "fail"
      rev_res.status = 0
    rev_res.save
    end
  else 
puts "Record not found"
  end
  
end



def pin_hash(pin)
    Digest::SHA256.hexdigest(pin)
end

def subscriber_id
id = Time.new.strftime("%Y%m%d%H%M%S%L") # Get current date to the milliseconds
    id = id.to_i.to_s(36)
end

def pin_save(msisdn,pin,subscription_id)
  PinTable.create(
                     
                     pin: pin_hash(DEFAULT_PIN),
                     msisdn: msisdn,
                     subscription_id: subscription_id,
                     alt_status: 0
                      
        )
        
       # pin_success  =  { resp_code: '000',resp_desc: 'PIN saved successfully'}
  
  #format_response(pin_success, request.accept)  
end

def virtual_wallet( first_name,last_name,mobile_number,amount)
  CustomerVirtualWallet.create(first_name:first_name,last_name:last_name,mobile_number: mobile_number,amount: 0.00,status:1,alt_status:0)

    # virtual_success  =  { resp_code: '000',resp_desc: 'Virtual Wallet created Successfully'}
#   
  # format_response(virtual_success, request.accept)  
end 


def savePaystate(trans_info, trans_id, trans_ref, trans_msg, amfp_status)
  PaymentState.create!(network_status: trans_info, trans_id: trans_id, trans_ref: trans_ref,
                       trans_msg: trans_msg, amfp_status: amfp_status, status: 1)
end 

def insert_subscribe(msisdn, first_name,last_name,alt_mobile_number,subsciber_code,pin,telco,auth_code)
  Subscription.create(msisdn: msisdn,first_name: first_name, last_name: last_name, alt_mobile_number: alt_mobile_number, subsciber_code: subsciber_code,pin: pin_hash(DEFAULT_PIN),status:1,verified: 0,telco: telco,auth_code: auth_code)
  
  # regis_success  =  { resp_code: '000',resp_desc: 'Registration on ALEXpay successfully done'}
#   
  # format_response(regis_success, request.accept)  

end

def TranSaver(customer_number, amount, u_code, nw,recipient_number,recipient_telco,sender_name,voucher_code,credit_amount,trans_type,master_code)
  time = Time.now.strftime('%Y-%m-%d %H:%M:%S')

  TransactionSummary.create(mobile_number: customer_number, amount: amount,
                            ack_code: u_code, telco: nw,
                            created_at: time,recipient_number: recipient_number,recipient_telco: recipient_telco,sender_name: sender_name,voucher_code: voucher_code,credit_amount: credit_amount , trans_type: trans_type,master_code: master_code)
end


def saveSendALex(customer_number, amount, u_code, nw,recipient_number,recipient_telco,sender_name,voucher_code,trans_type)
  time = Time.now.strftime('%Y-%m-%d %H:%M:%S')

  SendAlexSummary.create(mobile_number: customer_number, amount: amount,
                            ack_code: u_code, telco: nw,
                            created_at: time,recipient_number: recipient_number,recipient_telco: recipient_telco,sender_name: sender_name,voucher_code: voucher_code, trans_type: trans_type)
end



def format_response(data, accept)
  accept.each do |type|
    return data.to_xml  if type.downcase.eql? 'text/xml'
    return data.to_json if type.downcase.eql? 'application/json'
    return data.to_yaml if type.downcase.eql? 'text/x-yaml'
    return data.to_csv  if type.downcase.eql? 'text/csv'

    return data.to_json
  end
end

def genUniqueCode
  time=Time.new
  strtm=time.strftime("%y%m%d%L%H%M")
  return strtm
end

def uniqueCode
  time=Time.new
  strtm = time.strftime("ALX"+"%y%m%d%L%H%M")
  return strtm
end

def mobileMoneyReq(merchant_number, customer_number, amount, reference, callback_url, client_id, nw, trans_type,v_num,recipient_number,recipient_telco,sender_name,credit_amount,master_code)
 
  u_code=genUniqueCode
  
  url = 'https://184.173.139.74:8215'
 
    url_endpoint = '/sendRequest'
   
    

  
  conn = Faraday.new(url: url, headers: REQHDR, ssl: { verify: false }) do |f|
          f.response :logger
          f.adapter Faraday.default_adapter
  end
 
 puts
puts "START SAVING INTO TRANASCTION TABLE..."
puts 
 
     TranSaver(customer_number,amount, u_code, nw,recipient_number,recipient_telco,sender_name,v_num,credit_amount,trans_type, master_code)

ts=Time.now.strftime("%Y-%m-%d %H:%M:%S") 


puts
puts "STARTING PAYLOAD..."
puts

payload={
     :customer_number => customer_number,
     :reference => STRREF,
     :amount => amount,
     :exttrid => u_code,
     :nw => nw, 
     :trans_type => trans_type,
     :callback_url => callback_url, 
     :ts => ts,
     :client_id => client_id,
     :voucher_code=> v_num
 }

puts
puts "AFTER PAYLOAD..."
puts

json_payload=JSON.generate(payload)
msg_endpoint="#{json_payload}"

puts 
puts "JSON payload: #{json_payload}"
puts

def computeSignature(secret, data)
  digest=OpenSSL::Digest.new('sha256')
  signature = OpenSSL::HMAC.hexdigest(digest, secret.to_s, data)
  return signature
end
  
signature=computeSignature(SECRET_KEY, json_payload)

#begin
   res = conn.post do |req|
          req.url url_endpoint
          req.options.timeout = 30           # open/read timeout in seconds
          req.options.open_timeout = 30      # connection open timeout in seconds
          req['Authorization'] = "#{CLIENT_KEY}:#{signature}"
          req.body = json_payload
   end
        puts
        puts "Result from AMFP: #{res.body}"
        puts

end

def balance_enough?(amount, network)
    resp = check_wallet_balance
    bal = resp["balance"][network]["Withdrawal"].split[1].to_f

  puts "THE BAL IS... #{bal}"
  puts "THE NETWORK IS... #{network}"
  puts "THE response  IS... #{resp}"

    bal > amount.to_f
end


def check_wallet_balance
    url='https://184.173.139.74:8215'
    url_endpoint='/check_wallet_bal'
    time = Time.now.strftime("%Y-%m-%d %H:%M:%S")

    begin

        con = Faraday.new(:url => url, :headers => REQHDR, :ssl => {:verify => false}) do |faraday|
            faraday.response :logger # log requests to STDOUT
            faraday.adapter Faraday.default_adapter # make requests with Net::HTTP
        end

        params = {
            :client_id => CLIENT_ID,
            :ts => time
        }

        json_params = JSON.generate(params)
        string_json = "#{json_params}"
        
        def computeSignature(secret, data)
      digest=OpenSSL::Digest.new('sha256')
        signature = OpenSSL::HMAC.hexdigest(digest, secret.to_s, data)
      return signature
    end

        signature = computeSignature(SECRET_KEY, string_json)
        result = con.post do |request|
            request.url url_endpoint
            request.options.timeout = 180 # open/read timeout in seconds
            request.options.open_timeout = 180 # connection open timeout in seconds
            request["Authorization"]="#{CLIENT_KEY}:#{signature}"
            request.body = json_params
        end

        if result.status.to_s == "200"

            puts
            puts "::::::::::::::::::::::::::::::----  CHECK BALANCE --STATUS OK...  ----::::::::::::::::::::::::::::::::"
            puts

        end
        puts
        puts "Response: #{result.status} :: #{result.body}"
        puts
        return JSON.parse(result.body)

    rescue Faraday::SSLError
        puts
        puts "There was an issue sending the https request...."
        puts
    rescue Faraday::TimeoutError
        puts "Connection timeout error."

    rescue Exception => e
        puts "Error:"
        puts e.message
    end

end

def masterCode
  time=Time.new
  strtm = time.strftime("ALX"+"%y%m%d%L%H%M")
  return strtm
end

def compute_charge(amount)

   debit_amount = (amount *0.027) + amount
    puts "LETS SEE THE AMOUNT IN THE COMPUTE CHARGE FUNCTION #{amount}"
   
    puts "LETS SEE THE CALCULATED AMOUNT IN THE COMPUTE CHARGE FUNCTION #{debit_amount}"
    
    net_fee = (debit_amount * 100).floor / 100.0
    
    puts "LETS SEE THE CALCULATED AMOUNT IN THE COMPUTE CHARGE IN 2 DP #{net_fee}"
   
   return net_fee.to_f
     
 end




def processPayment(mobile_number,network,credit_amount,recipient_number,recipient_network,sender_name,voucher_code) 
  u_code=genUniqueCode
  
  master_code = masterCode
  
   debit_amount = compute_charge(credit_amount.to_f)
  
 
 if  balance_enough?(credit_amount, recipient_network)
      if network == "MTN"
       
         puts "using MTN"
        # Thread.new {
    
            mobileMoneyReq(MTN_MERCHANT,mobile_number, debit_amount, STRREF, CALLBACK_URL, CLIENT_ID, network, TRANS_TYPE_DEBIT,voucher_code,recipient_number,recipient_network,sender_name,credit_amount,master_code)
       #}
        elsif network == "VOD"
         
        #  Thread.new {
       
            mobileMoneyReq(VOD_MERCHANT, mobile_number, debit_amount, STRREF, CALLBACK_URL, CLIENT_ID, network,  TRANS_TYPE_DEBIT,voucher_code,recipient_number,recipient_network,sender_name,credit_amount,master_code)
        #}
        elsif network == "TIG"
        
          puts "using tigo"
          #Thread.new {
          mobileMoneyReq(TIG_MERCHANT, mobile_number, debit_amount, STRREF, CALLBACK_URL, CLIENT_ID, network, TRANS_TYPE_DEBIT,voucher_code,recipient_number,recipient_network,sender_name,credit_amount,master_code)
        #}  
        elsif network == "AIR"
         
          puts "using airtel"
        #  Thread.new {
           mobileMoneyReq(MERCHANT_NO, mobile_number, debit_amount, STRREF, CALLBACK_URL, CLIENT_ID, network, TRANS_TYPE_DEBIT,voucher_code,recipient_number,recipient_network,sender_name,credit_amount,master_code)
        #}
        end


  return 0
  
   else
       
          trans_fail  =  { resp_code: '011',resp_desc: 'Insufficient Balance in ALEXpay Wallet'}
     end  

trans_fail.to_json
end


def send_sms(sender_id, recipient_number, message_body)
  
  p resp = Message.create!(msg_id: sender_id, phone_num: recipient_number, msg: message_body)
 
 u_code=uniqueCode
 
 url = 'https://184.173.139.74:8215'
 
  url_endpoint = '/sendSms' 
  
  conn = Faraday.new(url: url, headers: REQHDR, ssl: { verify: false }) do |f|
          f.response :logger
          f.adapter Faraday.default_adapter
  end
  
    api_params = {
       "sender_id": sender_id ,
        "recipient_number": recipient_number,
        "message_body": message_body,
       "unique_id": u_code,
        "src": API,
        "api_key": API_KEY,
        "client_id": 22
      }

 api_params = api_params.to_json
      
      
 def computeSignature(secret, data)
  digest=OpenSSL::Digest.new('sha256')
  signature = OpenSSL::HMAC.hexdigest(digest, secret.to_s, data)
  return signature
end
  
signature=computeSignature(SECRET_KEY, api_params)

  res = conn.post do |req|
          req.url url_endpoint
          req.options.timeout = 30           # open/read timeout in seconds
          req.options.open_timeout = 30      # connection open timeout in seconds
          req['Authorization'] = "#{CLIENT_KEY}:#{signature}"
          req.body = api_params
   end
        puts
        puts "Result from TEST: #{res.body}"
        puts
     


end


def VirtualTrans(customer_number,recipient_number, amount, u_code, nw,voucher_code,trans_type,sender_name)
  time = Time.now.strftime('%Y-%m-%d %H:%M:%S')

  VirtualTransaction.create(mobile_number: customer_number,recipient_number: recipient_number,amount: amount,
                            ack_code: u_code, telco: nw,
                            status: '1', created_at: time,voucher_code: voucher_code,trans_type: trans_type,sender_name: sender_name)
end

def virtualTrans2(customer_number,recipient_number, amount, u_code, nw,voucher_code,trans_type)
  time = Time.now.strftime('%Y-%m-%d %H:%M:%S')

  VirtualTransaction.create(mobile_number: customer_number,recipient_number: recipient_number,amount: amount,
                            ack_code: u_code, telco: nw,
                            status: '1', created_at: time,voucher_code: voucher_code,trans_type: trans_type)
end


######### Virtual Transfers Functions ##################
def virtualMobileMoneyReq(merchant_number, customer_number, amount, reference, callback_url, client_id, nw, trans_type,v_num,sender_name,recipient_number,recipient_network)
  
 
  u_code=genUniqueCode
  
  url = 'https://184.173.139.74:8215'
  url_endpoint = '/sendRequest'

  
  conn = Faraday.new(url: url, headers: REQHDR, ssl: { verify: false }) do |f| 
          f.response :logger
          f.adapter Faraday.default_adapter
  end
  
  if trans_type == "DR"
   VirtualTrans(customer_number,sender_name,amount, u_code, nw,v_num,"Deposit",recipient_number)
  else
    VirtualTrans(customer_number,sender_name,amount, u_code, nw,v_num,"WIthdrawal",recipient_number) 
  end
  
ts=Time.now.strftime("%Y-%m-%d %H:%M:%S") 


puts
puts "Hello world..."
puts

payload={
     :customer_number => customer_number,
     :reference => STRREF,
     :amount => amount,
     :exttrid => u_code,
     :nw => nw,
     :trans_type => trans_type,
     :callback_url => callback_url,
     :ts => ts,
     :client_id => client_id,
     :voucher_code=> v_num
 }

puts
puts "Hello world Two..."
puts

json_payload=JSON.generate(payload)
msg_endpoint="#{json_payload}"

puts
puts "JSON payload: #{json_payload}"
puts

def computeSignature(secret, data)
  digest=OpenSSL::Digest.new('sha256')
  signature = OpenSSL::HMAC.hexdigest(digest, secret.to_s, data)
  return signature
end
  
signature=computeSignature(SECRET_KEY, json_payload)

#begin
   res = conn.post do |req|
          req.url url_endpoint
          req.options.timeout = 30           # open/read timeout in seconds
          req.options.open_timeout = 30      # connection open timeout in seconds
          req['Authorization'] = "#{CLIENT_KEY}:#{signature}"
          req.body = json_payload
   end
        puts
        puts "Result from AMFP: #{res.body}"
        puts

end




def sendAlexReq(merchant_number, customer_number, amount, reference, callback_url, client_id, nw, trans_type,v_num,recipient_number,recipient_telco,sender_name)
 
  u_code=genUniqueCode
  
  url = 'https://184.173.139.74:8215'
  url_endpoint = '/sendRequest'

  
  conn = Faraday.new(url: url, headers: REQHDR, ssl: { verify: false }) do |f| 
          f.response :logger
          f.adapter Faraday.default_adapter
  end

    saveSendALex(customer_number,amount, u_code, nw,recipient_number,recipient_telco,sender_name,v_num,trans_type)
ts=Time.now.strftime("%Y-%m-%d %H:%M:%S") 


puts
puts "Hello world..."
puts

payload={
     :customer_number => customer_number,
     :reference => STRREF,
     :amount => amount,
     :exttrid => u_code,
     :nw => nw,
     :trans_type => trans_type,
     :callback_url => callback_url,
     :ts => ts,
     :client_id => client_id,
     :voucher_code=> v_num
 }

puts
puts "Hello world Two..."
puts

json_payload=JSON.generate(payload)
msg_endpoint="#{json_payload}"

puts
puts "JSON payload: #{json_payload}"
puts

def computeSignature(secret, data)
  digest=OpenSSL::Digest.new('sha256')
  signature = OpenSSL::HMAC.hexdigest(digest, secret.to_s, data)
  return signature
end
  
signature=computeSignature(SECRET_KEY, json_payload)

#begin
   res = conn.post do |req|
          req.url url_endpoint
          req.options.timeout = 30           # open/read timeout in seconds
          req.options.open_timeout = 30      # connection open timeout in seconds
          req['Authorization'] = "#{CLIENT_KEY}:#{signature}"
          req.body = json_payload
   end
        puts
        puts "Result from AMFP: #{res.body}"
        puts

end


def sendAlexReq2(merchant_number, customer_number, amount, reference, callback_url, client_id, nw, trans_type,v_num,recipient_number,recipient_telco,sender_name)
 
  u_code=genUniqueCode
  
  url = 'https://184.173.139.74:8215'
  url_endpoint = '/sendRequest'

  
  conn = Faraday.new(url: url, headers: REQHDR, ssl: { verify: false }) do |f| 
          f.response :logger
          f.adapter Faraday.default_adapter
  end

    saveSendALex(customer_number,amount, u_code, nw,recipient_number,recipient_telco,sender_name,v_num,trans_type)
ts=Time.now.strftime("%Y-%m-%d %H:%M:%S") 


puts
puts "Hello world..."
puts

payload={
     :customer_number => customer_number,
     :reference => STRREF,
     :amount => amount,
     :exttrid => u_code,
     :nw => nw,
     :trans_type => trans_type,
     :callback_url => callback_url,
     :ts => ts,
     :client_id => client_id,
     :voucher_code=> v_num
 }

puts
puts "Hello world Two..."
puts

json_payload=JSON.generate(payload)
msg_endpoint="#{json_payload}"

puts
puts "JSON payload: #{json_payload}"
puts

def computeSignature(secret, data)
  digest=OpenSSL::Digest.new('sha256')
  signature = OpenSSL::HMAC.hexdigest(digest, secret.to_s, data)
  return signature
end
  
signature=computeSignature(SECRET_KEY, json_payload)

#begin
   res = conn.post do |req|
          req.url url_endpoint
          req.options.timeout = 30           # open/read timeout in seconds
          req.options.open_timeout = 30      # connection open timeout in seconds
          req['Authorization'] = "#{CLIENT_KEY}:#{signature}"
          req.body = json_payload
   end
        puts
        puts "Result from AMFP: #{res.body}"
        puts

end


def virtualprocessPayment(mobile_number,network,amount,sender_name,voucher_code) 
  u_code=genUniqueCode
  
  recipient_number = ""
  recipient_network = ""
  
  
  if  balance_enough?(amount, network)
    
  if network == "MTN"
    
         puts "using MTN"
         #Thread.new {
            virtualMobileMoneyReq(MTN_MERCHANT, mobile_number, amount, STRREF, VIR_CALLBACK_URL, CLIENT_ID, network, TRANS_TYPE_DEBIT,voucher_code,sender_name,recipient_number,recipient_network)
       #}
      elsif network == "VOD" 
         
          puts "using vodafone"
        
          #Thread.new {
       
            virtualMobileMoneyReq(VOD_MERCHANT, mobile_number, amount, STRREF, VIR_CALLBACK_URL, CLIENT_ID, network, TRANS_TYPE_DEBIT,voucher_code,sender_name,recipient_number,recipient_network)
            
       # }
       elsif network == "TIG"
         
          puts "using tigo"
         # Thread.new {
          virtualMobileMoneyReq(TIG_MERCHANT, mobile_number, amount, STRREF, VIR_CALLBACK_URL, CLIENT_ID, network, TRANS_TYPE_DEBIT,voucher_code,sender_name,recipient_number,recipient_network)
        #}  
        elsif network == "AIR"
         
          puts "using airtel"
         # Thread.new {
           virtualMobileMoneyReq(MERCHANT_NO, mobile_number, amount, STRREF, VIR_CALLBACK_URL, CLIENT_ID, network, TRANS_TYPE_DEBIT,voucher_code,sender_name,recipient_number,recipient_network)
        #}
        end
    else
       
          trans_fail  =  { resp_code: '011',resp_desc: 'Insufficient Balance in ALEXpay Wallet'}
     end  

end



def sendOtherALex(mobile_number,network,amount,recipient_number,recipient_network,sender_name,voucher_code)
  u_code=genUniqueCode
  
  
  if  balance_enough?(amount, network)
    
  if network == "MTN"
    
         puts "using MTN"
         #sendAlexReq(MTN_MERCHANT,mobile_number, amount, STRREF, SEND_ALEX_CALLBACK, CLIENT_ID, network, TRANS_TYPE_DEBIT,voucher_code,recipient_number,recipient_network,sender_name)
          virtualMobileMoneyReq(MTN_MERCHANT, mobile_number, amount, STRREF, SEND_ALEX_CALLBACK, CLIENT_ID, network, TRANS_TYPE_DEBIT,voucher_code,sender_name,recipient_number,recipient_network)
        
      elsif network == "VOD" 
         
          puts "using vodafone"
        
          #Thread.new {
         # sendAlexReq(VOD_MERCHANT,mobile_number, amount, STRREF, SEND_ALEX_CALLBACK, CLIENT_ID, network, TRANS_TYPE_DEBIT,voucher_code,recipient_number,recipient_network,sender_name)
          virtualMobileMoneyReq(VOD_MERCHANT, mobile_number, amount, STRREF, SEND_ALEX_CALLBACK, CLIENT_ID, network, TRANS_TYPE_DEBIT,voucher_code,sender_name,recipient_number,recipient_network)
       
       elsif network == "TIG"
         
          puts "using tigo"
         # Thread.new {
         #sendAlexReq(TIG_MERCHANT,mobile_number, amount, STRREF, SEND_ALEX_CALLBACK, CLIENT_ID, network, TRANS_TYPE_DEBIT,voucher_code,recipient_number,recipient_network,sender_name)
         virtualMobileMoneyReq(TIG_MERCHANT, mobile_number, amount, STRREF, SEND_ALEX_CALLBACK, CLIENT_ID, network, TRANS_TYPE_DEBIT,voucher_code,sender_name,recipient_number,recipient_network)
         
         
        elsif network == "AIR"
         
          puts "using airtel"
         # Thread.new {
         #sendAlexReq(MERCHANT_NO,mobile_number, amount, STRREF, SEND_ALEX_CALLBACK, CLIENT_ID, network, TRANS_TYPE_DEBIT,voucher_code,recipient_number,recipient_network,sender_name)
         virtualMobileMoneyReq(MERCHANT_NO, mobile_number, amount, STRREF, SEND_ALEX_CALLBACK, CLIENT_ID, network, TRANS_TYPE_DEBIT,voucher_code,sender_name,recipient_number,recipient_network)
        end
    else
       
          trans_fail  =  { resp_code: '011',resp_desc: 'Insufficient Balance in ALEXpay Wallet'}
     end  

end





def sendOtherALex2(mobile_number,network,amount,recipient_number,recipient_network,sender_name,voucher_code)
  u_code=genUniqueCode
  
  
  if  balance_enough?(amount, network)
    
    
    
     old_amount = CustomerVirtualWallet.where(mobile_number: mobile_number)[0] 
      puts "Sender Virtual balance is #{old_amount.amount}"
      
   if old_amount.amount == 0.00 or old_amount.amount < 1.00
   
         trans_fail  =  { resp_code: '011',resp_desc: 'our ALEXpay Account balance is insufficient to withdraw. Please re-imburse. '}     
    trans_fail.to_json
        strText3 = "Your ALEXpay Account balance is insufficient to withdraw. Please re-imburse. "
       puts send_sms(MSGSENDERID, mobile_number, strText3)
     
   else
      new_amount = old_amount.amount - res.amount.to_f
   
     CustomerVirtualWallet.where(mobile_number: mobile_number).update(:amount => new_amount)
  
  
  if network == "MTN"
    
         puts "using MTN"
         #Thread.new {
        # sendAlexReq2(MTN_MERCHANT,recipient_number, amount, STRREF, SEND_ALEX_CALLBACK2, CLIENT_ID, recipient_network, TRANS_TYPE_CREDIT,voucher_code,mobile_number,network,sender_name)
          virtualMobileMoneyReq(MTN_MERCHANT, recipient_number, amount, STRREF, SEND_ALEX_CALLBACK2, CLIENT_ID, recipient_network, TRANS_TYPE_CREDIT,voucher_code,sender_name,mobile_number,network)
           
       #}
      elsif network == "VOD" 
         
          puts "using vodafone"
        
          #Thread.new {
         # sendAlexReq2(VOD_MERCHANT,recipient_number, amount, STRREF, SEND_ALEX_CALLBACK2, CLIENT_ID, recipient_network, TRANS_TYPE_CREDIT,voucher_code,mobile_number,network,sender_name)
       
           virtualMobileMoneyReq(VOD_MERCHANT, recipient_number, amount, STRREF, SEND_ALEX_CALLBACK2, CLIENT_ID, recipient_network, TRANS_TYPE_CREDIT,voucher_code,sender_name,mobile_number,network)
       # }
       elsif network == "TIG"
         
          puts "using tigo"
         # Thread.new {
         #sendAlexReq2(TIG_MERCHANT,recipient_number, amount, STRREF, SEND_ALEX_CALLBACK2, CLIENT_ID, recipient_network, TRANS_TYPE_CREDIT,voucher_code,mobile_number,network,sender_name)
         virtualMobileMoneyReq(TIG_MERCHANT, recipient_number, amount, STRREF, SEND_ALEX_CALLBACK2, CLIENT_ID, recipient_network, TRANS_TYPE_CREDIT,voucher_code,sender_name,mobile_number,network)
        #}  
        elsif network == "AIR"
         
          puts "using airtel"
         # Thread.new {
        # sendAlexReq2(MERCHANT_NO,recipient_number, amount, STRREF, SEND_ALEX_CALLBACK2, CLIENT_ID, recipient_network, TRANS_TYPE_CREDIT,voucher_code,mobile_number,network,sender_name)
           virtualMobileMoneyReq(MERCHANT_NO, recipient_number, amount, STRREF, SEND_ALEX_CALLBACK2, CLIENT_ID, recipient_network, TRANS_TYPE_CREDIT,voucher_code,sender_name,mobile_number,network)
        #}
        end
    end
    
    else
       
          trans_fail  =  { resp_code: '011',resp_desc: 'Insufficient Balance in ALEXpay Wallet'}
     end  

end





def third_party_processPayment(mobile_number,network,amount,recipient_number,recipient_network,sender_name,voucher_code) 
   u_code=genUniqueCode
  
  virtualTrans2(mobile_number,recipient_number, amount, u_code, network,voucher_code,"Third-party")

   
    old_amount = CustomerVirtualWallet.where(mobile_number: mobile_number)[0] 
     rec_old_amount = CustomerVirtualWallet.where(mobile_number: recipient_number)[0] 
     puts "Sender Virtual balance is #{old_amount.amount}"
      puts "Recipient OLd balance is #{rec_old_amount.amount}"
    
     if old_amount.amount == 0.00 or old_amount.amount < 1
        strText3 = "Your ALEXpay Account balance is insufficient to withdraw. Please re-imburse. "
       #strMainMenu2= "Your virtual wallet balance is insufficient to deposit.Your current balance is #{old_amount.amount}.  Please top-up. "
      puts send_sms(MSGSENDERID, mobile_number, strText3)
     else
     
     new_amount = old_amount.amount - amount.to_f
     rec_new_amount = rec_old_amount.amount + amount.to_f
     
       puts "Sender NEW AMOUNT is #{new_amount}"
       puts "Recipient NEW AMOUNT is #{rec_new_amount}"

    
     CustomerVirtualWallet.where(mobile_number: recipient_number).update(:amount => rec_new_amount)
     CustomerVirtualWallet.where(mobile_number: mobile_number).update(:amount => new_amount)
    
      strText2 = "You have successfully deposited GHC#{amount} to #{recipient_number}.Your new virtual account balance is #{new_amount}. Your transaction id is: #{u_code}"
      strText4 = "You have received GHC#{amount} from #{sender_name} with number #{mobile_number}.Your new virtual account balance is #{rec_new_amount}. The transaction id is: #{u_code}"
      
      puts send_sms(MSGSENDERID, mobile_number, strText2)
      puts send_sms(MSGSENDERID, recipient_number, strText4)
     # strMainMenu2= "You have successfully deposited GHC#{amount} to#{recipient_number}.Your new virtual account balance is #{new_amount}. Your transaction id is: #{u_code}"
      end
 
end
 
 

 
 
 

def self_withdrawal_virtual(mobile_number,network,amount,sender_name,voucher_code)
  u_code=genUniqueCode
  
  recipient_number = ""
  recipient_network = ""
  
  if  balance_enough?(amount, network)
    
    
     old_amount = CustomerVirtualWallet.where(mobile_number: mobile_number)[0] 
      puts "Sender Virtual balance is #{old_amount.amount}"
      
   if old_amount.amount == 0.00 or old_amount.amount < 1.00
   
   trans_fail  =  { resp_code: '011',resp_desc: 'Your ALEXpay Account balance is insufficient to withdraw. Please re-imburse. '}     
    trans_fail.to_json
        strText3 = "Your ALEXpay Account balance is insufficient to withdraw. Please re-imburse. "
       puts send_sms(MSGSENDERID, mobile_number, strText3)
     
   else
      new_amount = old_amount.amount - res.amount.to_f
   
     CustomerVirtualWallet.where(mobile_number: mobile_number).update(:amount => new_amount)
    

     if network == "MTN"
       
         puts "using MTN"
        # Thread.new {
            virtualMobileMoneyReq(MTN_MERCHANT, mobile_number, amount, STRREF, W_VIR_CALLBACK_URL, CLIENT_ID, network, TRANS_TYPE_CREDIT,voucher_code,sender_name,recipient_number,recipient_network)
       #}
      elsif network == "VOD" 
         
          
          puts "using vodafone"
          
          #Thread.new {
       
            virtualMobileMoneyReq(VOD_MERCHANT, mobile_number, amount, STRREF, W_VIR_CALLBACK_URL, CLIENT_ID, network, TRANS_TYPE_CREDIT,voucher_code,sender_name,recipient_number,recipient_network)
       # }
        elsif network == "TIG" 
         
          puts "using tigo"
          Thread.new {
          virtualMobileMoneyReq(TIG_MERCHANT, mobile_number, amount, STRREF, W_VIR_CALLBACK_URL, CLIENT_ID, network, TRANS_TYPE_CREDIT,voucher_code,sender_name,recipient_number,recipient_network)
        }  
        elsif network == "AIR"
         
          puts "using airtel"
          Thread.new {
           virtualMobileMoneyReq(MERCHANT_NO, mobile_number, amount, STRREF, W_VIR_CALLBACK_URL, CLIENT_ID, network, TRANS_TYPE_CREDIT,voucher_code,sender_name,recipient_number,recipient_network)
        }
        end
     end   
         else
       
          trans_fail  =  { resp_code: '011',resp_desc: 'Insufficient Balance in ALEXpay Wallet'}
     end  

   
end

def topUpTrans(customer_number,recipient_number, amount, u_code, nw,voucher_code,trans_type,top_up_option,sender_name) 
  time = Time.now.strftime('%Y-%m-%d %H:%M:%S')  

     TopUpTransaction.create(mobile_number: customer_number,recipient_number: recipient_number,amount: amount,
                            ack_code: u_code, telco: nw,
                            status: '1', created_at: time,voucher_code: voucher_code,trans_type: trans_type,top_up_option: top_up_option,sender_name: sender_name)                         
end

def topUpTrans2(customer_number,recipient_number, amount, u_code, nw,voucher_code,trans_type,top_up_option,sender_name) 
  time = Time.now.strftime('%Y-%m-%d %H:%M:%S')  

     TopTransaction.create(mobile_number: customer_number,recipient_number: recipient_number,amount: amount,
                            ack_code: u_code, telco: nw,
                            status: '1', created_at: time,voucher_code: voucher_code,trans_type: trans_type,top_up_option: top_up_option,sender_name: sender_name)                         
end

def topUpMobileMoneyReq(merchant_number, customer_number, amount, reference, callback_url, client_id, nw, trans_type,v_num,recipient_number,top_up_option,sender_name)
 
  u_code=genUniqueCode
  
  url = 'https://184.173.139.74:8215'
  url_endpoint = '/sendRequest'

  
  conn = Faraday.new(url: url, headers: REQHDR, ssl: { verify: false }) do |f|
          f.response :logger
          f.adapter Faraday.default_adapter
  end
 
   topUpTrans(customer_number,recipient_number,amount, u_code, nw,v_num,trans_type,top_up_option,sender_name)
   
 
ts=Time.now.strftime("%Y-%m-%d %H:%M:%S") 


puts
puts "Hello world..."
puts

payload={
     :customer_number => customer_number,
     :reference => STRREF,
     :amount => amount,
     :exttrid => u_code,
     :nw => nw,
     :trans_type => trans_type,
     :callback_url => callback_url,
     :ts => ts,
     :client_id => client_id,
     :voucher_code=> v_num
 }

puts
puts "Hello world Two..."
puts

json_payload=JSON.generate(payload)
msg_endpoint="#{json_payload}"

puts
puts "JSON payload: #{json_payload}"
puts

def computeSignature(secret, data)
  digest=OpenSSL::Digest.new('sha256')
  signature = OpenSSL::HMAC.hexdigest(digest, secret.to_s, data)
  return signature
end
  
signature=computeSignature(SECRET_KEY, json_payload)

#begin
   res = conn.post do |req|
          req.url url_endpoint
          req.options.timeout = 30           # open/read timeout in seconds
          req.options.open_timeout = 30      # connection open timeout in seconds
          req['Authorization'] = "#{CLIENT_KEY}:#{signature}"
          req.body = json_payload
   end
        puts
        puts "Result from AMFP: #{res.body}"
        puts

end



def topUpMobileMoneyReq2(merchant_number, customer_number, amount, reference, callback_url, client_id, nw, trans_type,v_num,recipient_number,top_up_option,sender_name)
 
  u_code=genUniqueCode
  
  url = 'https://184.173.139.74:8215'
  url_endpoint = '/sendRequest'

  
  conn = Faraday.new(url: url, headers: REQHDR, ssl: { verify: false }) do |f|
          f.response :logger
          f.adapter Faraday.default_adapter
  end
 
   topUpTrans2(customer_number,recipient_number,amount, u_code, nw,v_num,trans_type,top_up_option,sender_name)
   
 
ts=Time.now.strftime("%Y-%m-%d %H:%M:%S") 


puts
puts "Hello world..."
puts

payload={
     :customer_number => customer_number,
     :reference => STRREF,
     :amount => amount,
     :exttrid => u_code,
     :nw => nw,
     :trans_type => trans_type,
     :callback_url => callback_url,
     :ts => ts,
     :client_id => client_id,
     :voucher_code=> v_num
 }

puts
puts "Hello world Two..."
puts

json_payload=JSON.generate(payload)
msg_endpoint="#{json_payload}"

puts
puts "JSON payload: #{json_payload}"
puts

def computeSignature(secret, data)
  digest=OpenSSL::Digest.new('sha256')
  signature = OpenSSL::HMAC.hexdigest(digest, secret.to_s, data)
  return signature
end
  
signature=computeSignature(SECRET_KEY, json_payload)

#begin
   res = conn.post do |req|
          req.url url_endpoint
          req.options.timeout = 30           # open/read timeout in seconds
          req.options.open_timeout = 30      # connection open timeout in seconds
          req['Authorization'] = "#{CLIENT_KEY}:#{signature}"
          req.body = json_payload
   end
        puts
        puts "Result from AMFP: #{res.body}"
        puts

end


def format_response(data, accept) 
  accept.each do |type|
    return data.to_xml  if type.downcase.eql? 'text/xml'
    return data.to_json if type.downcase.eql? 'application/json'
    return data.to_yaml if type.downcase.eql? 'text/x-yaml'
    return data.to_csv  if type.downcase.eql? 'text/csv'

   return data.to_json
  end
end



def self_topUpPayment(mobile_number,network,amount,sender_name,voucher_code,top_up_option)
  

 recipient_number = ""


     if network == "MTN"
      
         puts "using MTN"
         #Thread.new {
            topUpMobileMoneyReq(MTN_MERCHANT, mobile_number, amount, STRREF, TOP_CALLBACK_URL, CLIENT_ID, network, TRANS_TYPE_DEBIT,voucher_code,recipient_number,top_up_option,sender_name)
       #}
        elsif network == "VOD"
    
          puts "using vodafone"
        
          Thread.new {
       
           topUpMobileMoneyReq(VOD_MERCHANT, mobile_number, amount, STRREF, TOP_CALLBACK_URL, CLIENT_ID, network, TRANS_TYPE_DEBIT,voucher_code,recipient_number,top_up_option,sender_name)
        }
        elsif network == "TIG" 
        
          puts "using tigo"
          #Thread.new {
          topUpMobileMoneyReq(TIG_MERCHANT, mobile_number, amount, STRREF, TOP_CALLBACK_URL, CLIENT_ID, network, TRANS_TYPE_DEBIT,voucher_code,recipient_number,top_up_option,sender_name)
        #}  
        elsif network == "AIR"
       
          puts "using airtel"
          Thread.new {
           topUpMobileMoneyReq(MERCHANT_NO, mobile_number, amount, STRREF, TOP_CALLBACK_URL, CLIENT_ID, network, TRANS_TYPE_DEBIT,voucher_code,recipient_number,top_up_option,sender_name)
        }
        end
   
     
end

def other_topUpPayment(mobile_number,network,amount,recipient_number,sender_name,voucher_code,top_up_option)


     if network == "MTN"
    
         puts "using MTN"
         #Thread.new {
            topUpMobileMoneyReq(MTN_MERCHANT, mobile_number, amount, STRREF, TOP_CALLBACK_URL, CLIENT_ID, network, TRANS_TYPE_DEBIT,voucher_code,recipient_number,top_up_option,sender_name)
       #}
        elsif network == "VOD"
 
          puts "using vodafone"

          Thread.new {
         
       
           topUpMobileMoneyReq(VOD_MERCHANT, mobile_number, amount, STRREF, TOP_CALLBACK_URL, CLIENT_ID, network, TRANS_TYPE_DEBIT,voucher_code,recipient_number,top_up_option,sender_name)
        }
        elsif network == "TIG" 
         
          puts "using tigo"
          #Thread.new {
          topUpMobileMoneyReq(TIG_MERCHANT, mobile_number, amount, STRREF, TOP_CALLBACK_URL, CLIENT_ID, network, TRANS_TYPE_DEBIT,voucher_code,recipient_number,top_up_option,sender_name)
        #} 
         
        elsif network == "AIR"
       
          puts "using airtel"
          Thread.new {
           topUpMobileMoneyReq(MERCHANT_NO, mobile_number, amount, STRREF, TOP_CALLBACK_URL, CLIENT_ID, network, TRANS_TYPE_DEBIT,voucher_code,recipient_number,top_up_option,sender_name)
        }
        end 
 
end

def billPayment(mobile_number,network,amount,sender_name,voucher_code,bill_type,smart_card_num) 
  
     if network == "MTN"
      
         puts "using MTN"
         #Thread.new {
            billMobileMoneyReq(MTN_MERCHANT, mobile_number, amount, STRREF, BILL_CALLBACK_URL, CLIENT_ID, network, TRANS_TYPE_DEBIT,voucher_code,smart_card_num,bill_type,sender_name)
       #}
        elsif network == "VOD"
     
          puts "using vodafone"
  
          Thread.new {
  
           billMobileMoneyReq(VOD_MERCHANT, mobile_number, amount, STRREF, BILL_CALLBACK_URL, CLIENT_ID, network, TRANS_TYPE_DEBIT,voucher_code,smart_card_num,bill_type,sender_name)
        }
        elsif network == "TIG" 
         
          puts "using tigo"
          #Thread.new {
          billMobileMoneyReq(TIG_MERCHANT, mobile_number, amount, STRREF, BILL_CALLBACK_URL, CLIENT_ID,network, TRANS_TYPE_DEBIT,voucher_code,smart_card_num,bill_type,sender_name)
        #}  
        elsif network == "AIR"
        
          puts "using airtel"
          Thread.new {
           billMobileMoneyReq(MERCHANT_NO, mobile_number, amount, STRREF, BILL_CALLBACK_URL, CLIENT_ID, network, TRANS_TYPE_DEBIT,voucher_code,smart_card_num,bill_type,sender_name)
        }
        end 
end


def billMobileMoneyReq(merchant_number, customer_number, amount, reference, callback_url, client_id, nw, trans_type,v_num,smart_card_num,bill_type,sender_name)
 
  u_code=genUniqueCode
  
  url = 'https://184.173.139.74:8215'
  url_endpoint = '/sendRequest'

  
  conn = Faraday.new(url: url, headers: REQHDR, ssl: { verify: false }) do |f|
          f.response :logger
          f.adapter Faraday.default_adapter
  end
 
    billTrans(customer_number,amount, u_code, nw,v_num,trans_type,smart_card_num,bill_type,sender_name)
   
 
ts=Time.now.strftime("%Y-%m-%d %H:%M:%S") 


puts
puts "Hello world..."
puts

payload={
     :customer_number => customer_number,
     :reference => STRREF,
     :amount => amount,
     :exttrid => u_code,
     :nw => nw,
     :trans_type => trans_type,
     :callback_url => callback_url,
     :ts => ts,
     :client_id => client_id,
     :voucher_code=> v_num
 }

puts
puts "Hello world Two..."
puts

json_payload=JSON.generate(payload)
msg_endpoint="#{json_payload}"

puts
puts "JSON payload: #{json_payload}"
puts

def computeSignature(secret, data)
  digest=OpenSSL::Digest.new('sha256')
  signature = OpenSSL::HMAC.hexdigest(digest, secret.to_s, data) 
  return signature
end
  
signature=computeSignature(SECRET_KEY, json_payload)

#begin
   res = conn.post do |req|
          req.url url_endpoint
          req.options.timeout = 30           # open/read timeout in seconds
          req.options.open_timeout = 30      # connection open timeout in seconds
          req['Authorization'] = "#{CLIENT_KEY}:#{signature}"
          req.body = json_payload
   end
        puts
        puts "Result from AMFP: #{res.body}"
        puts

end


def billTrans(customer_number,amount, u_code, nw,voucher_code,trans_type,smart_card_num,bill_type,sender_name) 
  time = Time.now.strftime('%Y-%m-%d %H:%M:%S')  

     BillTransaction.create(mobile_number: customer_number,amount: amount,
                            ack_code: u_code, telco: nw,
                            status: '1', created_at: time,voucher_code: voucher_code,trans_type: trans_type,smart_card_num: smart_card_num,bill_type: bill_type,sender_name: sender_name)                         
end


def biillTrans(customer_number,amount, u_code, nw,voucher_code,trans_type,smart_card_num,bill_type,sender_name) 
  time = Time.now.strftime('%Y-%m-%d %H:%M:%S')  

     BiillTransaction.create(mobile_number: customer_number,amount: amount,
                            ack_code: u_code, telco: nw,
                            status: '1', created_at: time,voucher_code: voucher_code,trans_type: trans_type,smart_card_num: smart_card_num,bill_type: bill_type,sender_name: sender_name)                         
end


def billMobileMoneyReqC(merchant_number, customer_number, amount, reference, callback_url, client_id, nw, trans_type,v_num,recipient_number,smart_card_num,bill_type,sender_name)
 
  u_code=genUniqueCode
  
  url = 'https://184.173.139.74:8215'
   url_endpoint = '/testbillpay'
   #url_endpoint = '/sendRequest'

  
  conn = Faraday.new(url: url, headers: REQHDR, ssl: { verify: false }) do |f|
          f.response :logger
          f.adapter Faraday.default_adapter
  end
 
   if bill_type == "DST"
   biillTrans(customer_number,amount, u_code, nw,v_num,trans_type,smart_card_num,"DSTV",sender_name)
  elsif bill_type == "GOT" 
    biillTrans(customer_number,amount, u_code, nw,v_num,trans_type,smart_card_num,"GOTV",sender_name)
   end
 
ts=Time.now.strftime("%Y-%m-%d %H:%M:%S") 


puts
puts "STarting Payload in BIILL Transaction..." 
puts

payload={
     :customer_number => customer_number,
     :reference => STRREF,
     :amount => "1.00",
     :exttrid => u_code,
     :nw => bill_type,
     :trans_type => trans_type,
     :callback_url => callback_url,
     :ts => ts,
     :client_id => client_id,
     :voucher_code=> v_num,
     :account_number=> smart_card_num 
 }

puts
puts "ANOTHER PAYLOAD OUTPUT..."
puts

json_payload=JSON.generate(payload)
msg_endpoint="#{json_payload}"

puts
puts "JSON payload: #{json_payload}"
puts

def computeSignature(secret, data)
  digest=OpenSSL::Digest.new('sha256')
  signature = OpenSSL::HMAC.hexdigest(digest, secret.to_s, data)
  return signature
end
  
signature=computeSignature(SECRET_KEY, json_payload)

#begin
   res = conn.post do |req|
          req.url url_endpoint
          req.options.timeout = 30           # open/read timeout in seconds
          req.options.open_timeout = 30      # connection open timeout in seconds
          req['Authorization'] = "#{CLIENT_KEY}:#{signature}"
          req.body = json_payload
   end
        puts
        puts "Result from AMFP: #{res.body}"
        puts

end

def save_pin( msisdn,pin)
  PinTable.create(
                    
                     pin: pin_hash(pin),
                     msisdn: msisdn,
                     alt_status: 0
                     
        )
end




