require 'sinatra'
require 'json'
require 'nokogiri'
require 'sinatra/activerecord'
require 'yaml'
require 'faraday'
require 'net/http'
require 'uri'
require 'savon'
require 'digest/md5'
require 'openssl'
require './model'
require './functions'
require './callback'
require './process'
#require 'thread'

######## CONSTANTS ###############

MAINMENU="Welcome to AlexPay \n\n 1.Transfer Funds\n\n 2.My Profile"
SUB="Welcome to AlexPay \n\n1.Subscribe"
CHANGEPIN ="1.Change Pin"
PINMSG = "You have successfully changed your pin"
DEFAULT_PIN ="1234"
MAX_PIN = 4
SUBMSG="You have successfully subscribed! Default PIN is 1234.Please go to 'My profile' and change your pin."
TELCOSELECT="Select network \n\n1.Tigo\n2.MTN\n3.Vodafone\n4.Airtel"
VODMSG= "You need a voucher code in order to make payment. Please dial *110# and select 6 to generate a voucher code and return here to continue.\n\n1. Continue\n 2. Exit\n"
ENTERAMNT = "Please enter an amount to deposit"
ID_TYPE = "Please select an ID type \n\n 1. Voter's ID\n\n 2. Passport \n\n 3.Driver's License"
MSGSENDERID = "AlexPay"
STRTRANSFAILURE ="Sorry,your transaction failed"

INVALIDSELECTION = "Sorry. Wrong selection. Please try again."
MERCHANT_NUMBER = "261064828"
NETWRK = "AIR"
TRANS_TYPE_DEBIT ="DR"
TRANS_TYPE_CREDIT ="CR"
CALLBACK_URL = "http://67.205.74.208:8001/alex_pay_callback"
VIR_CALLBACK_URL = "http://67.205.74.208:8001/virtual_accounts_callback"
W_VIR_CALLBACK_URL = "http://67.205.74.208:8001/w_virtual_accounts_callback" 
THIRD_PARTY_CALLBACK ="http://67.205.74.208:8001/third_party_callback" 
CLIENT_ID = 22
SECRET_KEY ='W0Dz7jXYrkS3QJr5RpnwZGjY8i5PxzuHXhzmiMfLdnM9BePz4RDRPoQibIJ4OLxIOD2UUpRTujcGfTA4dB3Qtw=='
CLIENT_KEY = 'XlnBgzaj/4ZRseV3AratiyjVJeR75Hbhy0wq3y2XIO2bPNZ6jAct5XIC70oovj3kJf4oygF6kcgIzAbbwc7kvw=='
MERCHANT_NO = "261064828"
TIG_MERCHANT = "0271300376"
MTN_MERCHANT = ""
VOD_MERCHANT = ""


post '/' do
  request.body.rewind
  payload=JSON.parse(request.body.read)
  json_vals = payload
  sessionID=json_vals['session_id']
  msisdn=json_vals['msisdn']
  msg_type=json_vals['msg_type']
  ussd_body=json_vals['ussd_body'].strip if json_vals.has_key?('ussd_body')
  nw_code=json_vals['nw_code']
  service_code=json_vals['service_code']
  
  
   from_vodafone = ["release","Session timeout.","User timeout.","User timeout","Session timeout"]
     if from_vodafone.include?(ussd_body) && service_code.blank?
      s_params = Hash.new
      s_params['session_id'] = sessionID
      s_params['service_code'] = service_code
      s_params['msisdn'] = msisdn
      s_params['nw_code'] = nw_code
      s_params['msg_type'] = "2"
      s_params['ussd_body'] = ussd_body
      p s_params.to_json

   else
  process(sessionID,msisdn,ussd_body,nw_code,service_code,msg_type)
   end
end
  

def process(sessionID,mobile,ussd_body,nw_code,service_code,msg_type)
  s_params = {}
  s_params['session_id']=sessionID
  s_params['nw_code']=nw_code
  s_params['msisdn']=mobile
  s_params['service_code']=service_code

  subscribed_number = Subscription.where(:msisdn=>mobile).exists?
 if subscribed_number == true
 if msg_type == "0"
    
    if nw_code == "02" 
         
         function = "Voucher"
         page = 1
         saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
          voucher_confirm(sessionID,function, page,mobile,s_params)
     else
        
        function = "Menu"
        page = 1
        saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
        main_menu(sessionID, function, page, s_params, nw_code)

     end
   
 else  
      
        ####### TRANSFER PART  ###
         tracker = Track.where(session_id: sessionID).last
      if tracker.function == "Voucher" && tracker.page == 1 
          function = "Menu"
        page = 1
        saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
        main_menu(sessionID, function, page, s_params, nw_code)
        
      elsif tracker.function == "Menu" && tracker.page == 1 && ussd_body == "1"
            function = "Transfer"
            page = 2
            input = "Initial"
            saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
            transfer_funds(ussd_body,sessionID, function, page, s_params,input,nw_code,mobile)
      
       elsif tracker.function == "Transfer" && tracker.page == 2 && nw_code != "02"
            
            function = "Transfer"
            page = 3 
            input = "SelectTrans"
            saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
            transfer_funds(ussd_body,sessionID, function, page, s_params,input,nw_code,mobile) 
            
       elsif tracker.function == "Transfer" && tracker.page == 2 && nw_code == "02" && ussd_body == '2'
            
            function = "Transfer"
            page = 9 
            input = "Voucher"
            saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
            transfer_funds(ussd_body,sessionID, function, page, s_params,input,nw_code,mobile) 
            
      elsif tracker.function == "Transfer" && tracker.page == 2 && nw_code == "02" && ussd_body == '1'  
        
            function = "Transfer"
            page = 3 
            input = "SelectTrans"
            saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
            transfer_funds(ussd_body,sessionID, function, page, s_params,input,nw_code,mobile) 
              
       elsif tracker.function == "Transfer" && tracker.page == 9 && nw_code == "02"
          
            function = "Transfer"
            page = 3 
            input = "SelectTrans"
            saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
            transfer_funds(ussd_body,sessionID, function, page, s_params,input,nw_code,mobile) 
       elsif tracker.function == "Transfer" && tracker.page == 3
         
            function = "Transfer"
            page = 4
            input = "Deposits"
            saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
            transfer_funds(ussd_body,sessionID, function, page, s_params,input,nw_code,mobile) 
       
       elsif tracker.function == "Transfer" && tracker.page == 4
            
            function = "Transfer"
            page = 5    
            input = "Amount"
            saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
            transfer_funds(ussd_body,sessionID, function, page, s_params,input,nw_code,mobile) 
       elsif tracker.function == "Transfer" && tracker.page == 5 && nw_code != "02" 
            
            function = "Transfer"
            page = 6 
            input = "Summary" 
            saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
            transfer_funds(ussd_body,sessionID, function, page, s_params,input,nw_code,mobile) 
      elsif tracker.function == "Transfer" && tracker.page == 5 && nw_code == "02" 
            prev_strReply = UssdLog.where('session_id=? and page=?',
      sessionID, 4).select(:ussd_body)[0][:ussd_body]
      
      first_ussdb = UssdLog.where('session_id=? and page=?',
      sessionID, 5).select(:ussd_body)[0][:ussd_body] 
      if (prev_strReply == "1" and first_ussdb == "1") || (prev_strReply == "2" and first_ussdb == "1")
            function = "Transfer"
            page = 10 
            input = "Voucher" 
            saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
            transfer_funds(ussd_body,sessionID, function, page, s_params,input,nw_code,mobile)
      else
         function = "Transfer"
            page = 6 
            input = "Summary" 
            saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
            transfer_funds(ussd_body,sessionID, function, page, s_params,input,nw_code,mobile) 
      end
       elsif tracker.function == "Transfer" && tracker.page == 10 && nw_code == "02"
          
            function = "Transfer"
            page = 6 
            input = "Summary"
            saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
            transfer_funds(ussd_body,sessionID, function, page, s_params,input,nw_code,mobile) 
       elsif tracker.function == "Transfer" && tracker.page == 6
            
            function = "Transfer"
            page = 7
            input = "Summary2" 
            saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
            transfer_funds(ussd_body,sessionID, function, page, s_params,input,nw_code,mobile) 
       elsif tracker.function == "Transfer" && tracker.page == 7
            
            function = "Transfer"
            page = 8
            input = "Completed" 
            saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
            transfer_funds(ussd_body,sessionID, function, page, s_params,input,nw_code,mobile) 
            
        ### CHANGE PIN
        
      elsif tracker.function == "Menu" && tracker.page == 1 && ussd_body =="2"
  
            function = "Pin_change"
            page = 2
            input = "Selection"   
            saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
            change_profile(ussd_body,sessionID, function, page, s_params,input,nw_code,mobile)
       elsif tracker.function == "Pin_change" && tracker.page == 2 
        
            function = "Pin_change"
            page = 3
            input = "old_pin"
            saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
            change_profile(ussd_body,sessionID, function, page, s_params,input,nw_code,mobile)
       elsif tracker.function == "Pin_change" && tracker.page == 3
        
            function = "Pin_change"
            page = 4
            input = "new_pin"
            saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
            change_profile(ussd_body,sessionID, function, page, s_params,input,nw_code,mobile)
      elsif tracker.function == "Pin_change" && tracker.page == 4
        
            function = "Pin_change"
            page = 5
            input = "completed"
            saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
            change_profile(ussd_body,sessionID, function, page, s_params,input,nw_code,mobile)
       else
            wrongInput(s_params) 
      end
 
 end
  
  
else
    
    ## Subscription part
    if msg_type == "0"
        
         function = "Subscription"
         page = 1
         saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
         subscription(sessionID, function, page, s_params, nw_code)
         
    else  
     
        # ####### TRANSFER PART  ###
         tracker = Track.where(session_id: sessionID).last
      if tracker.function == "Subscription" && tracker.page == 1 && ussd_body == "1"
          function = "Registration"
           page = 2
           input = "first_name"
           saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
           registration(ussd_body,sessionID, function, page, s_params,input,nw_code,mobile)
      elsif tracker.function == "Registration" && tracker.page == 2 
        
            function = "Registration"
            page = 3
            input = "last_name"
            saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
            registration(ussd_body,sessionID, function, page, s_params,input,nw_code,mobile)
     elsif tracker.function == "Registration" && tracker.page == 3 
        
            function = "Registration"
            page = 4
            input = "alt_num"
            saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
            registration(ussd_body,sessionID, function, page, s_params,input,nw_code,mobile)
     elsif tracker.function == "Registration" && tracker.page == 4
        
            function = "Registration"
            page = 5
            input = "id_type"
            saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
            registration(ussd_body,sessionID, function, page, s_params,input,nw_code,mobile)
     elsif tracker.function == "Registration" && tracker.page == 5
        
            function = "Registration"
            page = 6
            input = "id_num"
            saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
            registration(ussd_body,sessionID, function, page, s_params,input,nw_code,mobile)
    elsif tracker.function == "Registration" && tracker.page == 6
        
            function = "Registration"
            page = 7
            input = "summary"
            saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
            registration(ussd_body,sessionID, function, page, s_params,input,nw_code,mobile)
    elsif tracker.function == "Registration" && tracker.page == 7
        
            function = "Registration"
            page = 8
            input = "summary2"
            saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
            registration(ussd_body,sessionID, function, page, s_params,input,nw_code,mobile)
    elsif tracker.function == "Registration" && tracker.page == 8 
        
            function = "Registration"
            page = 9 
            input = "summary3"
            saveUSSD(sessionID,mobile,msg_type,ussd_body,nw_code, service_code, page)
            registration(ussd_body,sessionID, function, page, s_params,input,nw_code,mobile)
    
     end   
end
end
end