def main_menu(session_id, function, page, s_params, network_code)
 if page == 1
     s_params['msg_type'] = "1"
  
     options = "Welcome to ALexPay\n\n"
     options << "1.Transfer Funds\n\n"
     options << "2.Change Profile\n\n"
     s_params['ussd_body'] = options
   
   saveTracker(page,function,session_id) 
 end

 p s_params.to_json
end

def subscription(session_id, function, page, s_params, network_code)
 if page == 1
     s_params['msg_type'] = "1"
  
     options = "Welcome to AlexPay\n\n" 
     options << "1.Subscribe\n\n"  
   
     s_params['ussd_body'] = options
    
   saveTracker(page,function,session_id)
 end

 p s_params.to_json
end


def voucher_confirm(session_id, function, page,mobile_num, s_params)
 if page == 1
    s_params['msg_type'] = "1"

    options = "You need a voucher code in order to make payment. Please dial *110# and select 6 to generate a voucher code and return here to continue.\n\n"
    options << "1. Continue\n"
    options << "2. Exit\n"
    s_params['ussd_body'] = options
    saveTracker(page,function,session_id)
  end

 p s_params.to_json

end

 


def transfer_funds(ussd_body,session_id, function, page, s_params,field,network_code,mobile)
 if page == 2 && field == "Initial" 
   if ussd_body == "00"
      function = "Menu"
        page = 1
        main_menu(session_id, function, page, s_params, network_code)
   elsif ussd_body == "1" 
     
      s_params['msg_type'] = "1"
      options = "1.Transfer to virtual Account\n\n2. Transfer to Networks \n\n00. Back" 
      s_params['ussd_body'] = options
      saveTracker(page,function,session_id)
    else  
      s_params['ussd_body'] = 'you chose a wrong option'
   end 
 
  elsif page == 3 && field == "SelectTrans"
    
    if ussd_body == "00"
      function = "Menu"
        page = 1
        main_menu(session_id, function, page, s_params, network_code)
   
    elsif ussd_body == '1'  || ussd_body == "9"
     s_params['msg_type'] = "1"
     options = "Virtual Wallet Account\n\n1.Deposit \n\n2. Withdraw \n\n 3.Check Account Balance \n\n00.Back"
     s_params['ussd_body'] = options
     saveTracker(page,function,session_id)
     
     elsif ussd_body == '2' || network_code == "02"
       
       if ussd_body == "00"
           function = "Transfer"
            page = 2
            input = "Initial"
            transfer_funds(ussd_body,session_id, function, page, s_params,input,network_code,mobile)
      else
        if  network_code == "02" ||  network_code == "03" || network_code == "06" ||  network_code == "01" 
     s_params['msg_type'] = "1"
     options = "Please Enter your Pin to Continue \n\n00.Back"
     s_params['ussd_body'] = options
     saveTracker(page,function,session_id)
     end
     end
     else
        s_params['ussd_body'] = "you entered a wrong option"
       
   end
   
   elsif page == 9 && field == "Voucher"
      if ussd_body == "00"
           function = "Transfer"
            page = 2
            input = "Initial" 
            transfer_funds(ussd_body,session_id, function, page, s_params,input,network_code,mobile)
      else
      s_params['msg_type'] = "1"
     options = "Please Enter voucher \n\n00.Back"
     s_params['ussd_body'] = options
     saveTracker(page,function,session_id)
      end 
      
       elsif page == 10 && field == "Voucher"
      if ussd_body == "00"
           function = "Transfer"
            page = 2
            input = "Initial" 
            transfer_funds(ussd_body,session_id, function, page, s_params,input,network_code,mobile)
      else
      s_params['msg_type'] = "1"
     options = "Please Enter voucher \n\n00.Back"
     s_params['ussd_body'] = options
     saveTracker(page,function,session_id)
      end 
     
     
  elsif page == 4 && field == "Deposits"  
    
      strReply = UssdLog.where('session_id=? and page=?',
      session_id, 3).select(:ussd_body)[0][:ussd_body]
      
      prev_strReply = UssdLog.where('session_id=? and page=?',
      session_id, 2).select(:ussd_body)[0][:ussd_body]
       
     if ussd_body == "00"
        
        s_params['msg_type'] = "1"
        options = "1.Transfer to virtual Account\n\n2. Transfer to Networks \n\n00. Back" 
        s_params['ussd_body'] = options
        page = 2
        saveTracker(page,function,session_id)
 
    elsif ussd_body == '1' || ussd_body == "8"
      
     s_params['msg_type'] = "1"
     options = "Deposit\n\n1.Self \n\n2. Third-party \n\n9.Back"
     s_params['ussd_body'] = options
     saveTracker(page,function,session_id)
     
     elsif ussd_body == "2" || ussd_body == "9"
        s_params['msg_type'] = "1"
     options = "Withdraw\n\n1.Self  \n\n9.Back"
     s_params['ussd_body'] = options
     saveTracker(page,function,session_id)
     
     elsif ussd_body == "3" 
    user_phone = UssdLog.where(' session_id=?', session_id).select(:msisdn)[0][:msisdn]
   old_amount = CustomerVirtualWallet.where(mobile_number: user_phone)[0]
        s_params['msg_type'] = "2"
     s_params['ussd_body']= "Your current balance in your virtual Account is #{old_amount.amount}\n"
     saveTracker(page,function,session_id)
     
     elsif (strReply== "2" and prev_strReply == "1") || network_code == "02"
        old_pin = PinTable.where(' msisdn=? and alt_status=?',mobile,0).order('id desc')[0].pin
          
          default_pin = Subscription.where(' msisdn=? and status=?',mobile,0).order('id desc')[0].pin

          old_pin2 = UssdLog.where(' session_id=?  and page=?',session_id, '4').order('id desc')[0].ussd_body
           
          puts "----------------"
          puts "----------------"
          puts "Old pin is #{old_pin} and old pin entered is #{old_pin2} and default pin is #{default_pin}"
          puts "----------------"
          if old_pin2.is_number?
            pin = validate_pin(old_pin2)
            if pin == "success"
              if pin_hash(old_pin2) == default_pin
      
                

                s_params['msg_type']= '2' 
                s_params['ussd_body']="Please Change Your default Pin before you continue.Go to MyProfile to change."
                saveTracker(page,function,session_id)
              elsif pin_hash(old_pin2) == old_pin
                

                s_params['msg_type']= '1'
                s_params['ussd_body']=TELCOSELECT
                saveTracker(page,function,session_id)

              else

                s_params['msg_type']= '2'
                s_params['ussd_body']="incorrect pin. Cant procceed. Try again"
                saveTracker(page,function,session_id)
              end
            elsif pin == "periodfail" || pin == "period_fail"
              s_params['msg_type'] = "2"
              s_params['ussd_body'] = "Dots are not allowed in the pin number"
              saveTracker(page,function,session_id)
            elsif pin == "fail"
              s_params['msg_type'] = "2"
              s_params['ussd_body'] = "Your pin number is supposed to be 4 digits. Please check and try again."
              saveTracker(page,function,session_id)
            end

          else
            s_params['msg_type'] = "2"
            s_params['ussd_body'] = "Not number"
            saveTracker(page,function,session_id)
          end 
      
       
    end
   
  elsif page == 5 && field == "Amount"   
      strReply = UssdLog.where('session_id=? and page=?',
      session_id, 3).select(:ussd_body)[0][:ussd_body]
     
      prev_strReply = UssdLog.where('session_id=? and page=?',
      session_id, 4).select(:ussd_body)[0][:ussd_body]
      
      first_ussdb = UssdLog.where('session_id=? and page=?',
      session_id, 2).select(:ussd_body)[0][:ussd_body]
     
      
       
     if (strReply == "1" and prev_strReply == "1")
       
      if ussd_body == "9"
           function = "Transfer"
            page = 3
            input = "SelectTrans" 
            transfer_funds(ussd_body,session_id, function, page, s_params,input,network_code,mobile)
        
      elsif ussd_body == "1"
       s_params['msg_type'] = "1"
     options = "Enter Amount(self) \n\n8.Back"
     s_params['ussd_body'] = options
     saveTracker(page,function,session_id)
     
     elsif ussd_body == "2"
      s_params['msg_type'] = "1"
      options = "Enter Recipient number to deposit \n\n8.Back"
      s_params['ussd_body'] = options
      saveTracker(page,function,session_id)
     end
    
    elsif (strReply == "1" and prev_strReply == "2")  
      if ussd_body == "9"
           function = "Transfer"
            page = 3
            input = "SelectTrans" 
            transfer_funds(ussd_body,session_id, function, page, s_params,input,network_code,mobile)
      elsif ussd_body == "1"
       s_params['msg_type'] = "1"
       options = "Enter Amount(withdraw) \n\n9.Back"
       s_params['ussd_body'] = options
       saveTracker(page,function,session_id)
     
      elsif ussd_body == "2"
       s_params['msg_type'] = "1"
       options = "Enter Recipient number to withdraw \n\n9.Back"
       s_params['ussd_body'] = options
       saveTracker(page,function,session_id)
      end
    elsif (strReply == "2" and first_ussdb == "1") || network_code == "02"
        s_params['msg_type'] = "1"
       options = "Enter Recipient number \n\n00.Back"
       s_params['ussd_body'] = options
       saveTracker(page,function,session_id)
      
    end
    
  elsif page == 6 && field == "Summary"
     strReply = UssdLog.where('session_id=? and page=?',
      session_id, 4).select(:ussd_body)[0][:ussd_body]
     
      prev_strReply = UssdLog.where('session_id=? and page=?',
      session_id, 5).select(:ussd_body)[0][:ussd_body]
      
      first_ussdb = UssdLog.where('session_id=? and page=?',
      session_id, 2).select(:ussd_body)[0][:ussd_body]
      
      other_ussdb = UssdLog.where('session_id=? and page=?',
      session_id, 3).select(:ussd_body)[0][:ussd_body]
      
      
      if ussd_body == "8" || ussd_body == "9"
           function = "Transfer"
            page = 4
            input = "Deposits" 
            transfer_funds(ussd_body,session_id, function, page, s_params,input,network_code,mobile)
      
      
       elsif (strReply == "1" and prev_strReply == "1") 
         
         
          
        
       s_params['msg_type'] = "1"
       options = self_dep_summary(session_id, mobile,network_code)
       s_params['ussd_body'] = options
       saveTracker(page,function,session_id)
     
       elsif  (strReply == "1" and prev_strReply == "2") 
        s_params['msg_type'] = "1"
        options = "Enter amount to deposit(third-party) \n\n9.Back"
        s_params['ussd_body'] = options
        saveTracker(page,function,session_id)
      
    
        elsif (strReply == "2" and prev_strReply == "1")  
      
         s_params['msg_type'] = "1"
        options = self_with_summary(session_id, mobile,network_code)
        s_params['ussd_body'] = options
        saveTracker(page,function,session_id)
     
    elsif (strReply == "2" and prev_strReply == "2")  
        s_params['msg_type'] = "1"
     options = "Enter amount number to withdraw(third-PArty) \n\n02.Back"
     s_params['ussd_body'] = options
     saveTracker(page,function,session_id)
     
   elsif (other_ussdb == "2" and first_ussdb == "1") || network_code == "02"
        s_params['msg_type'] = "1"
       options = "Enter Amount \n\n00.Back"
       s_params['ussd_body'] = options
       saveTracker(page,function,session_id)
    else
      
      s_params['msg_type'] = "2"
       options = "You entered a wrong option"
       s_params['ussd_body'] = options
     
    end
    
  elsif page == 7 && field == "Summary2"
    strReply = UssdLog.where('session_id=? and page=?',
      session_id, 4).select(:ussd_body)[0][:ussd_body]
     
      prev_strReply = UssdLog.where('session_id=? and page=?',
      session_id, 5).select(:ussd_body)[0][:ussd_body]
      
       first_ussdb = UssdLog.where('session_id=? and page=?',
      session_id, 2).select(:ussd_body)[0][:ussd_body]
      
      other_ussdb = UssdLog.where('session_id=? and page=?',
      session_id, 3).select(:ussd_body)[0][:ussd_body]
      
      if ussd_body == "00"
        
        s_params['msg_type'] = "1"
        options = "Enter Amount(self) \n\n8.Back"
        s_params['ussd_body'] = options
        page = 5
        saveTracker(page,function,session_id)
      elsif ussd_body == "9"
      s_params['msg_type'] = "1"
      options = "Enter Recipient number to deposit \n\n8.Back"
      s_params['ussd_body'] = options
      page = 5
      saveTracker(page,function,session_id)
       elsif ussd_body == "01"
      s_params['msg_type'] = "1"
       options = "Enter Amount(withdraw) \n\n9.Back"
       s_params['ussd_body'] = options
        page = 5
       saveTracker(page,function,session_id)
       elsif ussd_body == "02"
         
         s_params['msg_type'] = "1"
       options = "Enter Recipient number to withdraw \n\n9.Back"
       s_params['ussd_body'] = options
       page = 5
       saveTracker(page,function,session_id)
      
      elsif (strReply == "1" and prev_strReply == "1")
        
       s_params['msg_type'] = "2"
       options = virtualprocessPayment(ussd_body, session_id, mobile, page,network_code)
       s_params['ussd_body'] = options
       saveTracker(page,function,session_id)
     
       elsif  (strReply == "1" and prev_strReply == "2")
        s_params['msg_type'] = "1"
        options = thirdparty_dep_summary(session_id, mobile,network_code)
        s_params['ussd_body'] = options
        saveTracker(page,function,session_id)
      
    
    elsif (strReply == "2" and prev_strReply == "1")  
     
       s_params['msg_type'] = "2"
     options = virtualprocessPayment2(ussd_body, session_id, mobile, page,network_code)
     s_params['ussd_body'] = options
     saveTracker(page,function,session_id)
     
    elsif (strReply == "2" and prev_strReply == "2")  
        s_params['msg_type'] = "1"
     options = thirdparty_withdrawal_summary(session_id, mobile,network_code)
     s_params['ussd_body'] = options
     saveTracker(page,function,session_id)
     
      elsif (other_ussdb == "2" and first_ussdb == "1") || network_code == "02"
        s_params['msg_type'] = "1"
       options = previewEntries(session_id, mobile,network_code)
       s_params['ussd_body'] = options
       saveTracker(page,function,session_id)
      
     
    end
    
    elsif page == 8 && field == "Completed"
    strReply = UssdLog.where('session_id=? and page=?',
      session_id, 4).select(:ussd_body)[0][:ussd_body]
     
      prev_strReply = UssdLog.where('session_id=? and page=?',
      session_id, 5).select(:ussd_body)[0][:ussd_body]
      
        first_ussdb = UssdLog.where('session_id=? and page=?',
      session_id, 2).select(:ussd_body)[0][:ussd_body]
      
      other_ussdb = UssdLog.where('session_id=? and page=?',
      session_id, 3).select(:ussd_body)[0][:ussd_body]
      
      if ussd_body == "00"
        
        s_params['msg_type'] = "1"
        options = "Enter amount to deposit(third-party) \n\n9.Back"
        s_params['ussd_body'] = options
        page = 6
        saveTracker(page,function,session_id)
      elsif ussd_body == "01"
        
        s_params['msg_type'] = "1"
         options = "Enter amount number to withdraw(third-PArty) \n\n02.Back"
        s_params['ussd_body'] = options
        page = 6
        saveTracker(page,function,session_id)
      
      elsif (strReply == "1" and prev_strReply == "2") 
        
       s_params['msg_type'] = "2"
       options = third_party_processPayment(ussd_body, session_id, mobile, page,network_code)
       s_params['ussd_body'] = options
       saveTracker(page,function,session_id)
     
      elsif (strReply == "2" and prev_strReply == "2")  
      
       s_params['msg_type'] = "2"
       options = "THIRDPARTY WITHDRAWAL COMPLETED "
       s_params['ussd_body'] = options
       saveTracker(page,function,session_id)
       
       elsif (other_ussdb == "2" and first_ussdb == "1") || network_code == "02"
        s_params['msg_type'] = "2"
       options = processPayment(ussd_body, session_id, mobile, page,network_code)
       s_params['ussd_body'] = options
       saveTracker(page,function,session_id)
     
      end
 
   else
     s_params['ussd_body'] = 'You entered a wrong option' 
    end   
 p s_params.to_json
end


 
def change_profile(ussd_body,session_id, function, page, s_params,field,network_code,mobile)
  if page == 2 && field == "Selection" 
    
     
      
      s_params['msg_type'] = "1"
      options = "1.Change pin \n\n00. Back"  
      s_params['ussd_body'] = options
      saveTracker(page,function,session_id)
      
    
  elsif page == 3 && field == "old_pin"
    
      if ussd_body == "00"
        function = "Menu"
        page = 1
        main_menu(session_id, function, page, s_params, network_code)
        
      else

     s_params['msg_type'] = "1" 
     options = "Enter old pin \n\n00.Back"
     s_params['ussd_body'] = options
     saveTracker(page,function,session_id) 
     
     end
  elsif page == 4 && field == "new_pin" 
    if ussd_body == "00"
      
     function = "Pin_change" 
     page = 2
     input = "Selection"   
    
     change_profile(ussd_body,session_id, function, page, s_params,input,network_code,mobile)
    else
      
       old_pin = PinTable.where(' msisdn=? and alt_status=?',mobile,0).order('id desc')[0].pin
       old_pin2 = UssdLog.where(' session_id=?  and page=?',session_id, '4').order('id desc')[0].ussd_body
          puts "----------------"
          puts "----------------"
          puts "Old pin is #{old_pin} and old pin entered is #{old_pin2}"
          puts "----------------"
    if pin_hash(old_pin2) == old_pin
     s_params['msg_type'] = "1" 
     options = "Enter new pin \n\n1.Back"
     s_params['ussd_body'] = options
     saveTracker(page,function,session_id) 
     else
       s_params['msg_type']= '2'
       s_params['ussd_body']= " Old pin incorrect. Try again"
     end
  end
  elsif page == 5 && field == "completed"
    if ussd_body == "1"
     function = "Pin_change"
     page = 3
     input = "old_pin"   
     change_profile(ussd_body,session_id, function, page, s_params,input,network_code,mobile)
      
    else
      new_pin = UssdLog.where(' session_id=?  and page=?',session_id, '5').order('id desc')[0].ussd_body
          recipient_number = UssdLog.where(' session_id=?', session_id).select(:msisdn)[0][:msisdn]
          puts "----------------"
          puts "New pin is #{new_pin} "
          
          if new_pin.is_number?
            pin = validate_pin(new_pin) 
            if pin == "success"

          pin_save2(session_id, mobile,new_pin)

          strText2 ="Alert! Your pin has been changed. Your new pin is #{new_pin} "
          puts sendmsg(MSGSENDERID, recipient_number, strText2)

          

          s_params['msg_type']= '2'
          s_params['ussd_body']= PINMSG
          saveTracker(page,function,session_id)
          
          elsif pin == "periodfail" || pin == "period_fail"
              s_params['msg_type'] = "2"
              s_params['ussd_body'] = "Dots are not allowed in the pin number"
            elsif pin == "fail"
              s_params['msg_type'] = "2"
              s_params['ussd_body'] = "Your pin number is supposed to be 4 digits. Please check and try again."
            end
          
          else
            s_params['msg_type'] = "2"
            s_params['ussd_body'] = "Not number"
          end 
          

     # s_params['msg_type'] = "2"
     # options = "You have successfully changed your pin"
     # s_params['ussd_body'] = options
     # saveTracker(page,function,session_id)
      end
   else
     s_params['msg_type'] = "2"
     s_params['ussd_body'] = 'You entered a wrong option'
   end
    p s_params.to_json
end


def registration(ussd_body,session_id, function, page, s_params,field,network_code,mobile)
  if page == 2 && field == "first_name" 
     s_params['msg_type'] = "1"
      options = "Enter first name \n\n00. Back" 
      s_params['ussd_body'] = options
      saveTracker(page,function,session_id)
   elsif page == 3 && field == "last_name"
      s_params['msg_type'] = "1"
      options = "Enter last name \n\n00. Back" 
      s_params['ussd_body'] = options
      saveTracker(page,function,session_id) 
   elsif page == 4 && field == "alt_num"
      s_params['msg_type'] = "1"
      options = "Do you have any alternative number?\n 1.Yes\n 2.No \n\n00. Back" 
      s_params['ussd_body'] = options
      saveTracker(page,function,session_id) 
   elsif page == 5 && field == "id_type"
     alt = UssdLog.where('session_id=? and page=?',
      session_id, 5).select(:ussd_body)[0][:ussd_body]
     
     if alt== "1" 
       
      s_params['msg_type'] = "1"
      options = "Enter alternative number \n\n00. Back" 
      s_params['ussd_body'] = options
       saveTracker(page,function,session_id)
       
     elsif alt== "2"
      s_params['msg_type'] = "1"
      options = "Please select an ID type \n\n 1. Voter's ID\n\n 2. Passport \n\n 3.Driver's License \n\n00. Back" 
      s_params['ussd_body'] = options
      saveTracker(page,function,session_id) 
      
      end
   elsif page == 6 && field == "id_num"
     alt = UssdLog.where('session_id=? and page=?',
      session_id, 5).select(:ussd_body)[0][:ussd_body]
     
     if alt == "1"
      s_params['msg_type'] = "1"
      options = " Please select an ID type \n\n 1. Voter's ID\n\n 2. Passport \n\n 3.Driver's License\n\n00. Back" 
      s_params['ussd_body'] = options
      saveTracker(page,function,session_id) 
    
     elsif alt =="2"
      s_params['msg_type'] = "1"
      options = "Enter ID number\n\n00. Back" 
      s_params['ussd_body'] = options
      saveTracker(page,function,session_id) 
      end
   elsif page == 7 && field == "summary"
     alt = UssdLog.where('session_id=? and page=?',
      session_id, 5).select(:ussd_body)[0][:ussd_body]
     
     if alt == "1"
      s_params['msg_type'] = "1"
      options = " Enter ID number\n\n00. Back" 
      s_params['ussd_body'] = options
      saveTracker(page,function,session_id) 
      
       elsif alt =="2"
   
      s_params['msg_type'] = "1"
      options = previewSubscription_new(session_id,network_code)
      s_params['ussd_body'] = options
      saveTracker(page,function,session_id) 
      end
   elsif page == 8 && field == "summary2"
     alt = UssdLog.where('session_id=? and page=?',
      session_id, 5).select(:ussd_body)[0][:ussd_body]
     
     if alt == "1"  
      s_params['msg_type'] = "1"
      options = previewSubscription(session_id,network_code)
      s_params['ussd_body'] = options
      saveTracker(page,function,session_id) 
      elsif alt== "2" and ussd_body == "1"

      s_params['msg_type'] = "2"
      options = SUBMSG 
      s_params['ussd_body'] = options
      saveTracker(page,function,session_id) 
      
       strText2 ="You have successfully subscribed! Default PIN is 1234.Please go to 'My profile' and change your pin."
       recipient_number = UssdLog.where(' session_id=?', session_id).select(:msisdn)[0][:msisdn]   
          puts sendmsg(MSGSENDERID, recipient_number, strText2)

      first_name = UssdLog.where(' session_id=?  and page=?',session_id, '3').order('id desc')[0].ussd_body
      last_name= UssdLog.where(' session_id=?  and page=?',session_id, '4').order('id desc')[0].ussd_body
      id_type = id_type1(session_id) 
      id_num= UssdLog.where(' session_id=?  and page=?',session_id, '7').order('id asc')[0].ussd_body 
      _sub= subscriber_id
      subscriber(session_id, mobile, first_name,last_name,"",_sub,pin_hash(DEFAULT_PIN),id_type,id_num)
      pin_save(session_id, mobile,DEFAULT_PIN,_sub)  
        amount = 0.00 
      virtual_wallet(first_name,last_name,mobile,amount)  
      
      
      
      
      else
        
        if alt == "2" and ussd_body =="00"
          s_params['msg_type'] = "2"
       options = "you chose to cancel" 
       s_params['ussd_body'] = options
      saveTracker(page,function,session_id)
      end
      end
  elsif page == 9 && field == "summary3" 
     alt = UssdLog.where('session_id=? and page=?',
      session_id, 5).select(:ussd_body)[0][:ussd_body]
     
     if alt == "1" and ussd_body =="1"
      s_params['msg_type'] = "2"
      options = SUBMSG 
      s_params['ussd_body'] = options
      saveTracker(page,function,session_id)  
      
      strText2 ="You have successfully subscribed! Default PIN is 1234.Please go to 'My profile' and change your pin."
       recipient_number = UssdLog.where(' session_id=?', session_id).select(:msisdn)[0][:msisdn]   
          puts sendmsg(MSGSENDERID, recipient_number, strText2)

      first_name = UssdLog.where(' session_id=?  and page=?',session_id, '3').order('id desc')[0].ussd_body
      last_name= UssdLog.where(' session_id=?  and page=?',session_id, '4').order('id desc')[0].ussd_body
      alt_num = UssdLog.where(' session_id=?  and page=?',session_id, '6').order('id desc')[0].ussd_body 
      id_type = id_type(session_id) 
      id_num= UssdLog.where(' session_id=?  and page=?',session_id, '8').order('id asc')[0].ussd_body 
      _sub= subscriber_id
      subscriber(session_id, mobile, first_name,last_name,alt_num,_sub,pin_hash(DEFAULT_PIN),id_type,id_num)
      pin_save(session_id, mobile,DEFAULT_PIN,_sub)  
        amount = 0.00 
      virtual_wallet(first_name,last_name,mobile,amount)
      else
        if alt == "1" and ussd_body =="00"
             s_params['msg_type'] = "2"
        options = "you chose to cancel" 
      s_params['ussd_body'] = options
      saveTracker(page,function,session_id)
      end
      end
    
  end
  p s_params.to_json
end