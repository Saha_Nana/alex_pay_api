CREDIT_RETURN_URL = "http://67.205.74.208:6088/creditReturn"
CREDIT_TOP_URL = "http://67.205.74.208:6088/creditTopUp"
CREDIT_BILL_URL = "http://67.205.74.208:6088/creditbill"
SEND_ALEX_CALLBACK = "http://67.205.74.208:6088/send_alex_callback"  


post '/creditReturn' do
  request.body.rewind
  req = JSON.parse request.body.read
  puts req.inspect

  trans_id = req['trans_id']
  trans_status = req['trans_status']
  trans_ref = req['trans_ref']
  message = req['message']

  puts '---------PARAMETERS-------'
  puts trans_id.to_s
  puts trans_status.to_s
  puts trans_ref.to_s
  puts message.to_s

  trans_split = trans_status.split('/')
  trans_state = trans_split[0]
  network_state = trans_split[1]
  time = Time.new
  _updatetime = time.strftime('%y%m%d%H%M%S%L')
  
  
 
   tr = TransactionSummary.where(ack_code: trans_ref).select(:ack_code).count
   
   
   puts "---------------TransactionSmmary ---Tr #{tr}"

  if tr.to_i == 1
    
  
    
    savePaystate(network_state, trans_id, trans_ref, message, trans_state)

    res = TransactionSummary.where(ack_code: trans_ref).select(:amount, :recipient_number, :mobile_number, :telco, :recipient_telco,:sender_name, :voucher_code, :credit_amount, :master_code)[0]
    nw_code = res.telco
    
    puts "lets see the recipient#{res.recipient_number}and  mobile no #{res.mobile_number} and #{res.amount} and telco #{res.telco} and recipeint telco #{res.recipient_telco} and sender name #{res.sender_name} and voucher code #{res.voucher_code}"
   
    v_num =res.voucher_code
    
 if (network_state.to_s == '200') || (trans_state.to_s == '000')
   
     puts "-----------------------------------"
      puts "UPDATING STATUS TO SUCCESS IN CREDIT CALLBACK"
       puts "-----------------------------------"
       puts
       puts
         puts "LETS SEE THE TRANS REF #{trans_ref}"
         puts
    update_trans_status(trans_ref,"success")
      puts "-----------------------------------" 
      puts "FINISHED UPDATING IN CREDIT CALLBACK"
       puts "-----------------------------------"

      puts "***************************************"
      puts  "Success in Callback"
      puts "***************************************"
      
      strText = "You have received GHC#{res.amount} from #{res.sender_name} with mobile number #{res.recipient_number}.Your transaction id is: #{trans_ref} "
      strText2 = "You have successfully transfered GHC#{res.amount} to #{res.mobile_number}. Your transaction id is: #{trans_ref}"
      
      Thread.new {
        
        puts send_sms(MSGSENDERID, res.recipient_number, strText2)
        
      }
      
      Thread.new {
        puts send_sms(MSGSENDERID, res.mobile_number, strText)
        
      }
      
   
     
 
    else
      
       puts "-----------------------------------"
      puts "UPDATING STATUS TO FAIL IN CREDIT CALLBACK"
       puts "-----------------------------------"
       puts
      
      update_trans_status_cr(trans_ref,"fail")
     
     puts "**************Failure in Credit Callback*************************"
     puts "***************************************"
     puts "-----Mobile number is #{res.mobile_number}"
     puts "-----Recipient number is #{res.recipient_number}"
    
      puts send_sms(MSGSENDERID, res.recipient_number, "Sorry! Your funds transfer to other network failed. Please try again.")
     
    end
    
      resp = { resp_code: '000', resp_desc: 'Callback (CR) Success' }
  else
    resp = { resp_code: '01', resp_desc: 'Failure' }
  end
     
 

  puts "----Final  Response--------"
  puts resp.to_json
  puts
  return resp.to_json
 
end

post '/alex_pay_callback' do

  
  request.body.rewind
  req = JSON.parse request.body.read
  puts req.inspect

  trans_id = req['trans_id']
  trans_status = req['trans_status']
  trans_ref = req['trans_ref']
  message = req['message']

  puts '---------PARAMETERS-------'
  puts trans_id.to_s
  puts trans_status.to_s
  puts trans_ref.to_s
  puts message.to_s

  trans_split = trans_status.split('/')
  trans_state = trans_split[0]
  network_state = trans_split[1]
  time = Time.new
  _updatetime = time.strftime('%y%m%d%H%M%S%L')
  
  
   tr = TransactionSummary.where(ack_code: trans_ref).select(:ack_code).count
   
   
   puts "---------------TransactionSmmary ---Tr #{tr}"

  if tr.to_i == 1
    
    puts "-------Got Here-------"
  
    
    savePaystate(network_state, trans_id, trans_ref, message, trans_state)

    res = TransactionSummary.where(ack_code: trans_ref).select(:amount, :recipient_number, :mobile_number, :telco, :recipient_telco, :sender_name, :voucher_code, :credit_amount, :master_code)[0]
    nw_code = res.recipient_telco
    
    
    puts "lets see the callback mobile no #{res.recipient_number} and #{res.amount} and #{res.sender_name}"
   
  
    
 if (network_state.to_s == '200') || (trans_state.to_s == '000')
   
   
  puts  "Success in Callback"
       puts "-----------------------------------"
      puts "UPDATING STATUS TO SUCCESS"
       puts "-----------------------------------"
       
       puts "LETS SEE THE TRANS REF #{trans_ref}"
       update_trans_status(trans_ref,"success")
       
       puts "-----------------------------------"
       puts "FINISHED UPDATING...."
        puts "------------------------------------------"
      
     # if balance_enough?(res.amount, res.telco)
        
      if nw_code == "MTN"
        
        v_num ="n/a"
       
         puts "using MTN"
        #Thread.new {
             mobileMoneyReq(MTN_MERCHANT, res.recipient_number, res.credit_amount, STRREF, CREDIT_RETURN_URL, CLIENT_ID, res.recipient_telco, TRANS_TYPE_CREDIT,res.voucher_code,res.mobile_number,res.telco,res.sender_name,res.amount, res.master_code)
       #}
        elsif nw_code == "VOD"
          
          puts "using vodafone"
          
        #  Thread.new { 
       
            mobileMoneyReq(VOD_MERCHANT, res.recipient_number, res.credit_amount, STRREF, CREDIT_RETURN_URL, CLIENT_ID, res.recipient_telco, TRANS_TYPE_CREDIT,res.voucher_code,res.mobile_number,res.telco,res.sender_name,res.amount, res.master_code)
        #}
        elsif nw_code == "TIG" 
        
          puts "using tigo"
         # Thread.new {
             v_num ="n/a"
       
          mobileMoneyReq(TIG_MERCHANT, res.recipient_number, res.credit_amount, STRREF, CREDIT_RETURN_URL, CLIENT_ID, res.recipient_telco, TRANS_TYPE_CREDIT,res.voucher_code,res.mobile_number,res.telco,res.sender_name,res.amount, res.master_code)
       # }  
        elsif nw_code == "AIR"
          v_num ="n/a"
       
          puts "using airtel"
         # Thread.new {
          mobileMoneyReq(MERCHANT_NO, res.recipient_number, res.credit_amount, STRREF, CREDIT_RETURN_URL, CLIENT_ID, res.recipient_telco, TRANS_TYPE_CREDIT,res.voucher_code,res.mobile_number,res.telco,res.sender_name,res.amount, res.master_code)
        # }
        end
      
        
    else 
     
     puts "**************Failure in Callback*************************"
     puts "***************************************8"
      puts send_sms(MSGSENDERID, res.mobile_number, "Sorry your funds transfer to network failed. Please try again!")
     
    end
    
      resp = { resp_code: '000', resp_desc: 'Callback (DR) Success' }
  else
    resp = { resp_code: '01', resp_desc: 'Failure' }
  end
     


  puts "----Final  Response--------"
  puts resp.to_json
  puts
  return resp.to_json
 

end

post '/virtual_accounts_callback' do
  
   request.body.rewind
  req = JSON.parse request.body.read
  puts req.inspect

  trans_id = req['trans_id']
  trans_status = req['trans_status']
  trans_ref = req['trans_ref']
  message = req['message']

  puts '---------PARAMETERS-------'
  puts trans_id.to_s
  puts trans_status.to_s
  puts trans_ref.to_s
  puts message.to_s

  trans_split = trans_status.split('/')
  trans_state = trans_split[0]
  network_state = trans_split[1]
  time = Time.new
  _updatetime = time.strftime('%y%m%d%H%M%S%L')
  
  
   tr = VirtualTransaction.where(ack_code: trans_ref).select(:ack_code).count
   
   
   puts "---------------Virtual Transaction ---Tr #{tr}"

  if tr.to_i == 1
    
    puts "-------Got Here-------"
  
    
    savePaystate(network_state, trans_id, trans_ref, message, trans_state)

    res = VirtualTransaction.where(ack_code: trans_ref).select(:amount,:mobile_number, :telco, :voucher_code, :trans_type, :sender_name)[0]

    
    
    puts "lets see the callback mobile no #{res.mobile_number} and amount  #{res.amount}"
   
  
    
 if (network_state.to_s == '200') || (trans_state.to_s == '000')

      puts  "Success in Callback"
      
      
     old_amount = CustomerVirtualWallet.where(mobile_number: res.mobile_number)[0] 
     puts "OLd AMOUNT is #{old_amount.amount}"
     
     new_amount = old_amount.amount + res.amount
     
       puts "NEW AMOUNT is #{new_amount}"
     
     CustomerVirtualWallet.where(mobile_number: res.mobile_number).update(:amount => new_amount)
     
 
      strText2 = "You have successfully deposited GHC#{res.amount} to your virtual account.Your new virtual account is #{new_amount}. Your transaction id is: #{trans_ref}"
      
      puts send_sms(MSGSENDERID, res.mobile_number, strText2)
  
     
 
    else 
     
     puts "**************Failure in Callback*************************"
     puts "***************************************8"
      puts send_sms(MSGSENDERID, res.mobile_number, "Sorry! Deposit of GHC#{res.amount} to your virtual account failed. Please try again.")
     
    end
      resp = { resp_code: '00', resp_desc: 'Success' }
  else
    resp = { resp_code: '01', resp_desc: 'Failure' }
  end
     

  puts "----Final  Response--------"
  puts resp.to_json
  puts
  return resp.to_json
  
  
  
end



post '/send_alex_callback' do
  
   request.body.rewind
  req = JSON.parse request.body.read
  puts req.inspect

  trans_id = req['trans_id']
  trans_status = req['trans_status']
  trans_ref = req['trans_ref']
  message = req['message']

  puts '---------PARAMETERS-------'
  puts trans_id.to_s
  puts trans_status.to_s
  puts trans_ref.to_s
  puts message.to_s

  trans_split = trans_status.split('/')
  trans_state = trans_split[0]
  network_state = trans_split[1]
  time = Time.new
  _updatetime = time.strftime('%y%m%d%H%M%S%L')
  
  
   tr = SendAlexSummary.where(ack_code: trans_ref).select(:ack_code).count
   
   
   puts "---------------Virtual Transaction ---Tr #{tr}"

  if tr.to_i == 1
    
    puts "-------Got Here-------"
  
    
    savePaystate(network_state, trans_id, trans_ref, message, trans_state)

    res = SendAlexSummary.where(ack_code: trans_ref).select(:amount,:mobile_number, :telco, :voucher_code, :trans_type, :sender_name,:recipient_number)[0]

    
    
    puts "lets see the callback mobile no #{res.mobile_number} and amount  #{res.amount}"
   
  
    
 if (network_state.to_s == '200') || (trans_state.to_s == '000')

      puts  "Success in Callback"
      
      
     old_amount = CustomerVirtualWallet.where(mobile_number: res.recipient_number)[0] 
     puts "OLd AMOUNT is #{old_amount.amount}"
     
     new_amount = old_amount.amount + res.amount
     
       puts "NEW AMOUNT is #{new_amount}"
     
     CustomerVirtualWallet.where(mobile_number: res.recipient_number).update(:amount => new_amount)
     
 
      strText1 = "You have successfully transfered GHC#{res.amount} to the ALEXpay wallet of #{res.recipient_number}. Your transaction id is: #{trans_ref}"
       strText2 = "You have received GHC#{res.amount} in your ALEXpay wallet from #{res.mobile_number}. Your new ALEXpay wallet balance is #{new_amount}. Your transaction id is: #{trans_ref}"
      
      puts send_sms(MSGSENDERID, res.mobile_number, strText1)
       puts send_sms(MSGSENDERID, res.recipient_number, strText2)
  
     
 
    else 
     
     puts "**************Failure in Callback*************************"
     puts "***************************************8"
      puts send_sms(MSGSENDERID, res.mobile_number, "Sorry! transfer of GHC#{res.amount} to the ALEXpay wallet of #{res.recipient_number} failed. Please try again.")
     
    end
      resp = { resp_code: '00', resp_desc: 'Success' }
  else
    resp = { resp_code: '01', resp_desc: 'Failure' }
  end
     

  puts "----Final  Response--------"
  puts resp.to_json
  puts
  return resp.to_json
  
  
  
end



post '/send_alex_callback2' do
  
  request.body.rewind
  req = JSON.parse request.body.read
  puts req.inspect

  trans_id = req['trans_id']
  trans_status = req['trans_status']
  trans_ref = req['trans_ref']
  message = req['message']

  puts '---------PARAMETERS-------'
  puts trans_id.to_s
  puts trans_status.to_s
  puts trans_ref.to_s
  puts message.to_s

  trans_split = trans_status.split('/')
  trans_state = trans_split[0]
  network_state = trans_split[1]
  time = Time.new
  _updatetime = time.strftime('%y%m%d%H%M%S%L')
  
  
   tr = SendAlexSummary.where(ack_code: trans_ref).select(:ack_code).count
   puts "---------------Virtual Transaction ---Tr #{tr}"

 if tr.to_i == 1
    
    puts "-------Got Here-------"
  
    savePaystate(network_state, trans_id, trans_ref, message, trans_state)

    res = SendAlexSummary.where(ack_code: trans_ref).select(:amount,:mobile_number, :telco, :voucher_code, :trans_type, :sender_name,:recipient_number)[0]
    puts "lets see the callback mobile no #{res.mobile_number} and amount  #{res.amount}"
      
    if (network_state.to_s == '200') || (trans_state.to_s == '000')

      puts  "Success in Callback"
     
     old_amount = CustomerVirtualWallet.where(mobile_number: res.recipient_number)[0] 
     new_amount =  old_amount.amount
     
      strText2 = "You have successfully deposited GHC#{res.amount} to #{res.mobile_number}.Your new ALEXPay Account balance is GH#{new_amount}. Your transaction id is: #{trans_ref}"
      strText = "You have received GHC#{res.amount} from #{res.recipient_number} in your Mobile Money Account. Your transaction id is: #{trans_ref}"
     
      
      puts send_sms(MSGSENDERID, res.mobile_number, strText)
      puts send_sms(MSGSENDERID, res.recipient_number, strText2)
  
    else 
      puts "**************Failure in Callback*************************"
      puts send_sms(MSGSENDERID, res.recipient_number, "Sorry! transfer of GHC#{res.amount} to the ALEXpay wallet of #{res.mobile_number} failed. Please try again.")
    end
      resp = { resp_code: '00', resp_desc: 'Success' }
 else
      resp = { resp_code: '01', resp_desc: 'Failure' }
 end
     

     puts "----Final  Response--------"
     puts resp.to_json
     puts
     return resp.to_json
end




post '/w_virtual_accounts_callback' do
  
   request.body.rewind
  req = JSON.parse request.body.read
  puts req.inspect

  trans_id = req['trans_id']
  trans_status = req['trans_status']
  trans_ref = req['trans_ref']
  message = req['message']

  puts '---------PARAMETERS-------'
  puts trans_id.to_s
  puts trans_status.to_s
  puts trans_ref.to_s
  puts message.to_s

  trans_split = trans_status.split('/')
  trans_state = trans_split[0]
  network_state = trans_split[1]
  time = Time.new
  _updatetime = time.strftime('%y%m%d%H%M%S%L')
  
  
   tr = VirtualTransaction.where(ack_code: trans_ref).select(:ack_code).count
   
   
   puts "---------------Virtual Transaction ---Tr #{tr}"

  if tr.to_i == 1
    
    puts "-------Got Here-------"
  
    
    savePaystate(network_state, trans_id, trans_ref, message, trans_state)

    res = VirtualTransaction.where(ack_code: trans_ref).select(:amount,:mobile_number, :telco, :voucher_code, :trans_type)[0]
    #nw_code = res.recipient_telco
    
    
    puts "lets see the callback mobile no #{res.mobile_number} and amount  #{res.amount}"
   
  
    
 if (network_state.to_s == '200') || (trans_state.to_s == '000')

      puts  "Success in Callback"
      
      
     old_amount = CustomerVirtualWallet.where(mobile_number: res.mobile_number)[0] 
     new_amount =  old_amount.amount
    
      strText2 = "You have successfully withdrawn GHC#{res.amount} to your mobile money account.Your new virtual account balance is #{new_amount}. Your transaction id is: #{trans_ref}"
      
      puts send_sms(MSGSENDERID, res.mobile_number, strText2)
  


    else 
     
     puts "**************Failure in Callback*************************"
     puts "***************************************8"
      puts send_sms(MSGSENDERID, res.mobile_number, "Sorry! Withdrawal of GHC#{res.amount} to your mobile money account failed. Please try again.")
     
    end
      resp = { resp_code: '00', resp_desc: 'Success' }
  else
    resp = { resp_code: '01', resp_desc: 'Failure' }
  end
     

  puts "----Final  Response--------"
  puts resp.to_json
  puts
  return resp.to_json
  

end

post '/third_party_callback' do


  
  request.body.rewind
  req = JSON.parse request.body.read
  puts req.inspect

  trans_id = req['trans_id']
  trans_status = req['trans_status']
  trans_ref = req['trans_ref']
  message = req['message']

  puts '---------PARAMETERS-------'
  puts trans_id.to_s
  puts trans_status.to_s
  puts trans_ref.to_s
  puts message.to_s

  trans_split = trans_status.split('/')
  trans_state = trans_split[0]
  network_state = trans_split[1]
  time = Time.new
  _updatetime = time.strftime('%y%m%d%H%M%S%L')
  
  
   tr = TVirtualTransaction.where(ack_code: trans_ref).select(:ack_code).count
   
   
   puts "---------------TransactionSmmary ---Tr #{tr}"

  if tr.to_i == 1
    
    puts "-------Got Here-------"
  
    
    savePaystate(network_state, trans_id, trans_ref, message, trans_state)

    res = TVirtualTransaction.where(ack_code: trans_ref).select(:amount, :recipient_number, :mobile_number, :telco, :sender_name, :voucher_code)[0]
 
    
    
    puts "lets see the callback mobile no #{res.recipient_number} and #{res.amount} and #{res.sender_name}"
   
  
    
  if (network_state.to_s == '200') || (trans_state.to_s == '000')

      puts  "Success in Callback"
      
      
     old_amount = TVirtualTransaction.where(mobile_number: res.mobile_number)[0] 
     puts "OLd AMOUNT is #{old_amount.amount}"
     puts "VIrtual Wallet balance is #{old_amount.amount} "
     if old_amount.amount == 0.00 
        strText3 = "Your virtual wallet balance is insufficient to withdraw. Please top-up. "
      
      puts send_sms(MSGSENDERID, res.mobile_number, strText3)
     else
     
     new_amount = old_amount.amount + res.amount
     
       puts "NEW AMOUNT is #{new_amount}"
     
     TVirtualTransaction.where(mobile_number: res.mobile_number).update(:amount => new_amount)
    
      strText2 = "You have successfully withdrawn GHC#{res.amount} to your mobile money account. Your transaction id is: #{trans_ref}"
      
      puts send_sms(MSGSENDERID, res.mobile_number, strText2)
  
     end

    else 
     
     puts "**************Failure in Callback*************************"
     puts "***************************************8"
      puts send_sms(MSGSENDERID, res.mobile_number, "Sorry! Withdrawal of GHC#{res.amount} to your mobile money account failed. Please try again.")
     
    end
      resp = { resp_code: '00', resp_desc: 'Success' }
  else
    resp = { resp_code: '01', resp_desc: 'Failure' }
  end
     

  puts "----Final  Response--------"
  puts resp.to_json
  puts
  return resp.to_json
  
  
  
end

post '/topUp_callback' do   

    
  request.body.rewind
  req = JSON.parse request.body.read
  puts req.inspect

  trans_id = req['trans_id']
  trans_status = req['trans_status']
  trans_ref = req['trans_ref']
  message = req['message']  

  puts '---------PARAMETERS-------'
  puts trans_id.to_s
  puts trans_status.to_s
  puts trans_ref.to_s
  puts message.to_s

  trans_split = trans_status.split('/')
  trans_state = trans_split[0]
  network_state = trans_split[1]
  time = Time.new
  _updatetime = time.strftime('%y%m%d%H%M%S%L')
  
  
   tr = TopUpTransaction.where(ack_code: trans_ref).select(:ack_code).count
   
   
   puts "---------------TopUpTransaction ---Tr #{tr}"

  if tr.to_i == 1
    
    puts "-------Got Here-------"
  
    
    savePaystate(network_state, trans_id, trans_ref, message, trans_state)

    res = TopUpTransaction.where(ack_code: trans_ref).select(:amount, :recipient_number, :mobile_number, :telco, :recipient_telco, :sender_name, :voucher_code, :top_up_option)[0]
    nw_code = res.telco
    
    
    puts "lets see the callback mobile no #{res.recipient_number} or #{res.mobile_number}  and #{res.amount} and #{res.sender_name} and network is #{nw_code}" 
   
  
    
 if (network_state.to_s == '200') || (trans_state.to_s == '000')

      puts  "Success in Callback"
      
     # if balance_enough?(res.amount, res.telco)
        
      if nw_code == "MTN"
        
        v_num ="n/a" 
       
         puts "using MTN"
        #Thread.new {
            
       topUpMobileMoneyReq2(MTN_MERCHANT, res.mobile_number, res.amount, STRREF, CREDIT_TOP_URL, CLIENT_ID, nw_code, TRANS_TYPE_TP,v_num,res.recipient_number,res.top_up_option,res.sender_name)
       #}
        elsif nw_code == "VOD"
          
          puts "using vodafone"
           v_num =res.voucher_code
           puts "vocuher number in callback #{v_num}"
        #  Thread.new { 
       
             topUpMobileMoneyReq2(VOD_MERCHANT, res.mobile_number, res.amount, STRREF, CREDIT_TOP_URL, CLIENT_ID, nw_code, TRANS_TYPE_TP,v_num,res.recipient_number,res.top_up_option,res.sender_name)
        #}
        elsif nw_code == "TIG" 
        
          puts "using tigo"
         # Thread.new {
             v_num ="n/a"
       
           topUpMobileMoneyReq2(TIG_MERCHANT, res.mobile_number, res.amount, STRREF, CREDIT_TOP_URL, CLIENT_ID, nw_code, TRANS_TYPE_TP,v_num,res.recipient_number,res.top_up_option,res.sender_name)
       # }  
        elsif nw_code == "AIR"
          v_num ="n/a"
       
          puts "using airtel"
          Thread.new {
           topUpMobileMoneyReq2(MERCHANT_NO, res.mobile_number, res.amount, STRREF, CREDIT_TOP_URL, CLIENT_ID, nw_code, TRANS_TYPE_TP,v_num,res.recipient_number,res.top_up_option,res.sender_name)
         }
        end
      
        
    else 
     
     puts "**************Failure in Callback*************************"
     puts "***************************************8"
      puts send_sms(MSGSENDERID, res.mobile_number, "Sorry, your airtime top up transaction failed. Please try again.")
     
    end
    
      resp = { resp_code: '000', resp_desc: 'Success' }
  else
    resp = { resp_code: '01', resp_desc: 'Failure' }
  end
     


  puts "----Final  Response--------"
  puts resp.to_json
  puts
  return resp.to_json
  
end

post '/creditTopUp' do

  request.body.rewind
  req = JSON.parse request.body.read
  puts req.inspect

  trans_id = req['trans_id']
  trans_status = req['trans_status']
  trans_ref = req['trans_ref']
  message = req['message']

  puts '---------PARAMETERS-------'
  puts trans_id.to_s
  puts trans_status.to_s
  puts trans_ref.to_s
  puts message.to_s

  trans_split = trans_status.split('/')
  trans_state = trans_split[0]
  network_state = trans_split[1]
  time = Time.new
  _updatetime = time.strftime('%y%m%d%H%M%S%L')
  
  
 
   tr = TopTransaction.where(ack_code: trans_ref).select(:ack_code).count
   
   
   puts "---------------TopTransaction  CREDIT TOP UP ---Tr #{tr}"

  if tr.to_i == 1
    
  
    
    savePaystate(network_state, trans_id, trans_ref, message, trans_state)

    res = TopTransaction.where(ack_code: trans_ref).select(:amount, :recipient_number, :mobile_number, :telco, :recipient_telco,:sender_name, :voucher_code, :top_up_option)[0]
    nw_code = res.telco
    
    puts "lets see the recipient#{res.recipient_number}and  mobile no #{res.mobile_number} and #{res.amount} and telco #{res.telco} and recipeint telco #{res.recipient_telco} and sender name #{res.sender_name} and voucher code #{res.voucher_code} and top up option #{res.top_up_option}"
    
   
    v_num =res.voucher_code
    
 if (network_state.to_s == '200') || (trans_state.to_s == '000')

      puts "***************************************"
      puts  "Success in Callback"
      puts "***************************************"
      
    if res.top_up_option == "Self"
     
      strText = "Your Airtime top up of GHC#{res.amount} was successful . Your transaction id is: #{trans_ref}"
      
      puts send_sms(MSGSENDERID, res.mobile_number, strText)
       
      
     elsif  res.top_up_option == "Others"
   
     strText2 = "You have successfully transferred Airtime of GHC#{res.amount} to #{res.recipient_number} . Your transaction id is: #{trans_ref}"
      strText = "Your have received Airtime worth GHC#{res.amount}  phone number #{res.mobile_number}. Your transaction id is: #{trans_ref}"
      
      puts send_sms(MSGSENDERID, res.mobile_number, strText2)
      puts send_sms(MSGSENDERID, res.recipient_number, strText) 
      end
    else
     
     puts "**************Failure in Credit Callback*************************"
     puts "***************************************"
     puts "-----Mobile number is #{res.mobile_number}"
     puts "-----Recipient number is #{res.recipient_number}"
    
      puts send_sms(MSGSENDERID, res.mobile_number, "Sorry, your airtime top up transaction failed. Please try again.")
     
    end
    
      resp = { resp_code: '000', resp_desc: 'Success' }
  else
    resp = { resp_code: '01', resp_desc: 'Failure' }
  end
     
 

  puts "----Final  Response--------"
  puts resp.to_json
  puts
  return resp.to_json
 
end

post '/bill_callback' do   
    
  request.body.rewind
  req = JSON.parse request.body.read
  puts req.inspect

  trans_id = req['trans_id']
  trans_status = req['trans_status']
  trans_ref = req['trans_ref']
  message = req['message']  

  puts '---------PARAMETERS-------'
  puts trans_id.to_s
  puts trans_status.to_s
  puts trans_ref.to_s
  puts message.to_s

  trans_split = trans_status.split('/')
  trans_state = trans_split[0]
  network_state = trans_split[1]
  time = Time.new
  _updatetime = time.strftime('%y%m%d%H%M%S%L')
  
  
   tr = BillTransaction.where(ack_code: trans_ref).select(:ack_code).count
   
   
   puts "---------------BillTransaction ---Tr #{tr}"

  if tr.to_i == 1
    
    puts "-------INSIDE BILL DR CALLBACK-------"
  
    
    savePaystate(network_state, trans_id, trans_ref, message, trans_state)

    res = BillTransaction.where(ack_code: trans_ref).select(:amount, :recipient_number, :mobile_number, :telco, :recipient_telco, :sender_name, :voucher_code, :smart_card_num, :bill_type)[0]
    nw_code = res.telco
    
    
    puts "lets see the callback mobile no #{res.recipient_number} or #{res.mobile_number}  and #{res.amount} and #{res.sender_name} and network is #{nw_code}" 
   
  
    
 if (network_state.to_s == '200') || (trans_state.to_s == '000') 

      puts  "Success in Callback"
      
     # if balance_enough?(res.amount, res.telco)
        
      if nw_code == "MTN"
        
        v_num ="n/a" 
       
         puts "using MTN"
        Thread.new {
          
       billMobileMoneyReqC(MTN_MERCHANT, res.mobile_number, res.amount, STRREF, CREDIT_BILL_URL, CLIENT_ID, nw_code, TRANS_TYPE_BP,v_num,res.smart_card_num,res.bill_type,res.sender_name)
      }
        elsif nw_code == "VOD"
          
          puts "using vodafone"
           v_num =res.voucher_code
           puts "vocuher number in callback #{v_num}"
        #  Thread.new { 
       
            billMobileMoneyReqC(VOD_MERCHANT, res.mobile_number, res.amount, STRREF, CREDIT_BILL_URL, CLIENT_ID, nw_code, TRANS_TYPE_BP,v_num,res.smart_card_num,res.bill_type,res.sender_name)
        #}
        
        elsif nw_code == "TIG" 
        
          puts "using tigo"
         # Thread.new {
             v_num ="n/a"
       
           
          billMobileMoneyReqC(TIG_MERCHANT, res.mobile_number, res.amount, STRREF, CREDIT_BILL_URL, CLIENT_ID, nw_code, TRANS_TYPE_BP,v_num,res.smart_card_num,res.bill_type,res.sender_name)
       # }  
        elsif nw_code == "AIR"
          v_num ="n/a"
       
          puts "using airtel"
          Thread.new { 
           
        billMobileMoneyReqC(MERCHANT_NO, res.mobile_number, res.amount, STRREF, CREDIT_BILL_URL, CLIENT_ID, nw_code, TRANS_TYPE_BP,v_num,res.smart_card_num,res.bill_type,res.sender_name)
         }
        end
      
        
    else 
     
     puts "**************Failure in Callback*************************"
     puts "***************************************8"
      puts send_sms(MSGSENDERID, res.mobile_number, "Sorry! Your Bill payment failed. Please try again.")
     
    end
    
      resp = { resp_code: '000', resp_desc: 'Success' }
  else
    resp = { resp_code: '01', resp_desc: 'Failure' }
  end
     


  puts "----Final  Response--------"
  puts resp.to_json
  puts
  return resp.to_json
  
end

post '/creditbill' do
  request.body.rewind
  req = JSON.parse request.body.read
  puts req.inspect

  trans_id = req['trans_id']
  trans_status = req['trans_status']
  trans_ref = req['trans_ref']
  message = req['message']

  puts '---------PARAMETERS-------'
  puts trans_id.to_s
  puts trans_status.to_s
  puts trans_ref.to_s
  puts message.to_s

  trans_split = trans_status.split('/')
  trans_state = trans_split[0]
  network_state = trans_split[1]
  time = Time.new
  _updatetime = time.strftime('%y%m%d%H%M%S%L')
  
  
 
   tr = BiillTransaction.where(ack_code: trans_ref).select(:ack_code).count
   
   
   puts "---------------BiillTransaction  CREDIT BILL ---Tr #{tr}"

  if tr.to_i == 1
    
  
    
    savePaystate(network_state, trans_id, trans_ref, message, trans_state)

    res = BiillTransaction.where(ack_code: trans_ref).select(:amount, :recipient_number, :mobile_number, :telco, :recipient_telco,:sender_name, :voucher_code, :bill_type)[0]
    nw_code = res.telco
    
    puts "lets see the recipient#{res.recipient_number}and  mobile no #{res.mobile_number} and #{res.amount} and telco #{res.telco} and recipeint telco #{res.recipient_telco} and sender name #{res.sender_name} and voucher code #{res.voucher_code}"
   
    v_num =res.voucher_code
    
 if (network_state.to_s == '200') || (trans_state.to_s == '000')

      puts "***************************************"
      puts  "Success in Callback(Bill Payemnt)"
      puts "***************************************"
      
    if res.bill_type == "DSTV"
      
      strText2 = "Your payment of GHC#{res.amount} for your DSTV subscription has been succesful. Your transaction id is: #{trans_ref}"
      puts send_sms(MSGSENDERID, res.mobile_number, strText2)
      
    elsif res.bill_type == "GOTV" 
         strText2 = "Your payment of  GHC#{res.amount} for your GOTV subscription has been succesful. Your transaction id is: #{trans_ref}"
      puts send_sms(MSGSENDERID, res.mobile_number, strText2)
      
      # elsif res.bill_type == "ECG"
        # strText2 = "Your payment of  GHC#{res.amount} for ECG has been succesful. Your transaction id is: #{trans_ref}"
      # puts send_sms(MSGSENDERID, res.mobile_number, strText2)
      end   
      
    else
     
     puts "**************Failure in Credit Callback*************************"
     puts "***************************************"
     puts "-----Mobile number is #{res.mobile_number}"
     puts "-----Recipient number is #{res.recipient_number}"
      puts send_sms(MSGSENDERID, res.mobile_number, "Bill Payment service unavailable!")
     
    end
    
      resp = { resp_code: '000', resp_desc: 'Success' }
  else
    resp = { resp_code: '01', resp_desc: 'Failure' }
  end
     
 

  puts "----Final  Response--------"
  puts resp.to_json
  puts
  return resp.to_json
 
end
